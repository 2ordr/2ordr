﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace ToOrder.Web
{
    public class MyHub1 : Hub
    {
        public void Hello()
        {
            string tm = DateTime.Now.ToShortTimeString();

            Clients.All.introduce("Hello I'm from Hub" + tm);
        }
    }
}