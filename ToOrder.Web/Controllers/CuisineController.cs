﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Services.Interfaces;

namespace ToOrder.Web.Controllers
{

    public class CuisineController : Controller
    {
        ICuisineService _cuisineService;
        IRestaurantCuisineService _restaurantCuisineService;

        public CuisineController(ICuisineService cuisineService, IRestaurantCuisineService restaurantCuisineService)
        {
            _cuisineService = cuisineService;
            _restaurantCuisineService = restaurantCuisineService;
        }

        // GET: Cuisine
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult SearchByCuisine()
        {
            var cuisines = _cuisineService.GetAll().OrderBy(c => c.Name);
            return PartialView(cuisines);
        }

        public ActionResult ShowCuisines(int restaurantId)
        {
            var cuisines = _restaurantCuisineService.GetAll(c => c.RestaurantId == restaurantId);
            return PartialView("ShowCuisinesPartial", cuisines);
        }
    }
}