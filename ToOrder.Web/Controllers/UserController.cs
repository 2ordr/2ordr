﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mail;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Web.ViewModels;


namespace ToOrder.Web.Controllers
{
    //[Authorize(Roles="Admin")]
    public class UserController : BaseController
    {
        IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        // GET: User
        public ActionResult Index()
        {
            var users = _userService.GetAll();
            return View(users);
        }

        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            var user = _userService.GetById(id);
            return PartialView(user);
        }

        [Authorize]
        public ActionResult MyAccount()
        {
            var user = _userService.GetAll(u => string.Equals(u.Email, User.Identity.Name, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            return View(user);
        }

        // GET: User/Create
        public ActionResult Create()
        {
            UserSignUpVM user = new UserSignUpVM();
            return View(user);
        }

        // POST: User/Create
        [HttpPost]
        public ActionResult Create(UserSignUpVM user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    User newUser = new User()
                    {
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Email = user.Email,
                        Password = user.Password,
                        Mobile=user.Mobile
                    };

                    newUser.LastUpdated = System.DateTime.Now;
                    newUser.IsActivated = false;

                    string activationCode = Guid.NewGuid().ToString();
                    newUser.ActivationCode = activationCode;

                    _userService.AddNew(newUser);
                    SendActivationEmail(newUser);

                    return RedirectToAction("AccountCreated", new { id = newUser.Id });
                }
                return View(user);


            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View(user);
            }
        }

        public ActionResult AccountCreated(int id)
        {
            var user = _userService.GetById(id);
            return View(user);
        }


        private void SendActivationEmail(User user)
        {
            try
            {
                //todo: make it asynchronnious call and template based

                using (System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("sender@gmail.com", user.Email.ToString()))
                {

                    mm.Subject = "Account Activation";
                    string body = "Hello " + user.FirstName.Trim() + ",";
                    body += "<br /><br />Please click the following link to activate your account";
                    body += "<br /><a href = '" + Request.Url.AbsoluteUri.Replace("Create", "Activated?ActivationCode=" + user.ActivationCode + "&UserId=" + user.Id.ToString()) + "'>Click here to activate your account.</a>";
                    body += "<br /><br />Thanks";
                    mm.Body = body;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("manojgawas49@gmail.com", "papa2119");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }


            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }


        }
        public ActionResult Activated(string ActivationCode, string UserId)
        {
            var user = _userService.GetById(Convert.ToInt32(UserId));
            if (user.ActivationCode == ActivationCode)
            {
                user.IsActivated = true;
                _userService.Update(user);
                ViewBag.Message = "Account Activated Successfully";
                return View();
            }
            else
            {
                ViewBag.Message = "Invalid  Activation Code";
                return View();
            }
        }


        // GET: User/Edit/5
        public ActionResult Edit()
        {
            var user = GetAuthenticatedUser();
            return PartialView(user);
        }

        // POST: User/Edit/5

        [HttpPost]
        public ActionResult Edit(User user)
        {
            try
            {
                ModelState["Password"].Errors.Clear();




                if (ModelState.IsValid)
                {

                    var authenticatedUser = GetAuthenticatedUser();

                    authenticatedUser.FirstName = user.FirstName;
                    authenticatedUser.LastName = user.LastName;
                    authenticatedUser.DOB = user.DOB;
                    authenticatedUser.Mobile = user.Mobile;
                    authenticatedUser.Telephone = user.Telephone;
                    authenticatedUser.FaxNumber = user.FaxNumber;
                    authenticatedUser.Website = user.Website;
                    authenticatedUser.Address.Country = user.Address.Country;

                    _userService.Update(authenticatedUser);
                    return PartialView(user);
                }


                return PartialView(user);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return PartialView(user);
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete(int id)
        {
            var user = _userService.GetById(id);
            return View(user);
        }

        // POST: User/Delete/5
        [HttpPost]
        public ActionResult Delete(User user)
        {
            try
            {
                _userService.Delete(user);

                return RedirectToAction("Index");
            }
            catch
            {
                return View(user);
            }
        }

        [Authorize]
        public ActionResult ChangePassword()
        {
            ChangePassword changepassword = new ChangePassword();
            return PartialView(changepassword);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePassword changepassword)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    var user = GetAuthenticatedUser();
                    if (user.Password == changepassword.CurrentPassword)
                    {
                        user.Password = changepassword.Password;
                        _userService.Update(user);
                        return PartialView("PasswordChanged");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Incorrect Current Password");
                        return PartialView(changepassword);
                    }


                }

                return PartialView(changepassword);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return PartialView(changepassword);
            }
        }
    }
}
