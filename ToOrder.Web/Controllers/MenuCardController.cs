﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Data;
using ToOrder.Web.ViewModels;

namespace ToOrder.Web.Controllers
{
    public class MenuCardController : Controller
    {
        IMenuCardService _menucardService;
        IMenuItemService _menuitemService;
        ICategoryService _categoryService;
        IRestaurantService _restaurantService;

        public MenuCardController(IMenuCardService menucardService, IMenuItemService menuitemService, ICategoryService categoryService, IRestaurantService restaurantService)
        {
            _menucardService = menucardService;
            _menuitemService = menuitemService;
            _categoryService = categoryService;
            _restaurantService = restaurantService;
        }
        // GET: MenuCard
        public ActionResult Index(int restaurantId)
        {
            var items = _menuitemService.GetAll(i => i.MenuCard.IsDefault && i.MenuCard.RestaurantId == restaurantId).OrderBy(i => i.Category.Name);
            ViewBag.restaurantId = restaurantId;
            var restaurant = _restaurantService.GetById(restaurantId);
            HttpCookie cookie = new HttpCookie("userInfo");
            if (cookie != null)
            {
                cookie.Expires = DateTime.Now.AddYears(1);
                cookie.Values.Add("Locality", restaurant.Address.Locality);
                cookie.Values.Add("lattitude", restaurant.Latitude.ToString());
                cookie.Values.Add("longitude", restaurant.Longitude.ToString());
            }// update cookie value
            else
            {
                cookie = new HttpCookie("userInfo");
                cookie.Values.Add("Locality", restaurant.Address.Locality);
                cookie.Values.Add("lattitude", restaurant.Latitude.ToString());
                cookie.Values.Add("longitude", restaurant.Longitude.ToString());
                cookie.Expires = DateTime.Now.AddYears(1);
            }
            Response.Cookies.Add(cookie);
            return View(items.ToList());
        }


        public ActionResult SearchMenuItem(string keywords, int restaurantId)
        {
            var items = _menuitemService.GetAll(r => (keywords == null || r.Name.Contains(keywords)) && r.MenuCard.IsDefault && r.MenuCard.RestaurantId == restaurantId).OrderBy(i => i.Category.Name);
            ViewBag.restaurantId = restaurantId;

            return View("Index", items.ToList());
        }
    }
}