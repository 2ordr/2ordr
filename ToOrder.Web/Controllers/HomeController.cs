﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Core.Services;
using ToOrder.Core.Services.Interfaces;

namespace ToOrder.Web.Controllers
{
    public class HomeController : BaseController
    {
        IUserService _userService;
        public HomeController(IUserService userService)
        {
            _userService = userService;
        }

        
        public ActionResult Index()
        {
            
            return View();
            
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Help()
        {
            return View();
        }

        public ActionResult TermsCondition()
        {
            return View();
        }

        public ActionResult PartnerWithUs()
        {
            return View();
        }
    }
}