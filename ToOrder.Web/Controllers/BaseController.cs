﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Web.Helpers;

namespace ToOrder.Web.Controllers
{
    public class BaseController : Controller
    {

        private readonly IUserService _userService = DependencyResolver.Current.GetService<IUserService>();

        public   string cultureName = null;
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            // Attempt to read the culture cookie from Request
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                cultureName = cultureCookie.Value;
            }
            else
                cultureName = Request.UserLanguages != null && Request.UserLanguages.Length > 0 ? Request.UserLanguages[0] : null;     // obtain it from HTTP header AcceptLanguages

            //Validate culture name
            cultureName = CultureHelper.GetImplementedCulture(cultureName);

            //modify current thread's cultures
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            return base.BeginExecuteCore(callback, state);
        }

        
        public User GetAuthenticatedUser()
        {
            
            if(this.Request.IsAuthenticated)
            {
                return _userService.GetAll(u => String.Equals(u.Email, User.Identity.Name, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
 
            }

            return null;
        }

        
        

    }
}