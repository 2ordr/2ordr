﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Web.App_Start;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Data;

namespace ToOrder.Web.Controllers
{
    public class MenuItemController : BaseController
    {
        IUserFavoriteService _userFavoriteService;


        public MenuItemController(IUserFavoriteService userFavoriteService)
        {
            _userFavoriteService = userFavoriteService;
        }

        // GET: MenuItem
        public ActionResult Index()
        {
            return View();
        }

        //POST:MenuItem/SaveFavorite
        public ActionResult SaveFavorite(int id, string returnUrl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = GetAuthenticatedUser();

                    var userfavorite = _userFavoriteService.GetAll(k => k.MenuItemId == id && k.UserId == user.Id).FirstOrDefault();

                    if (userfavorite != null)
                    {
                        TempData["Message"] = " MenuItem already set as Favorite. !! ";

                        return Redirect(returnUrl);
                    }
                    else
                    {
                        UserFavorites userFavorite = new UserFavorites()
                        {
                            MenuItemId = id,
                            UserId = user.Id,
                        };
                        _userFavoriteService.AddNew(userFavorite);

                        TempData["Message"] = " Item added to Favorites ! ";

                        return Redirect(returnUrl);
                    }
                }
                else
                {
                    this.Response.StatusCode = 400;
                    return Redirect(returnUrl);
                }

            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                if (ex is InvalidOperationException)
                    ModelState.AddModelError("", "Please correct following errors:");
                else
                    ModelState.AddModelError("", ex.Message);
                return Redirect(returnUrl);
            }

        }

        public ActionResult Myfavorites()
        {
            var authenticatedUser = GetAuthenticatedUser();

            var menuItem = _userFavoriteService.GetAll(m => m.UserId == authenticatedUser.Id).OrderByDescending(m => m.Id).Take(12);

            return PartialView("Myfavorites", menuItem);
        }

        //POST:MenuItem/Delete/id
        public ActionResult Delete(int id, string returnUrl)
        {
            UserFavorites userFavorites = _userFavoriteService.GetById(id);

            _userFavoriteService.Delete(userFavorites);

            return Redirect(returnUrl);
        }

    }
}