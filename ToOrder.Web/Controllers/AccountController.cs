﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Web.ViewModels;

namespace ToOrder.Web.Controllers
{
    public class AccountController : BaseController
    {
        IUserService _userService;

        public AccountController(IUserService userService)
        {
            _userService = userService;
        }
        // GET: Account
        public ActionResult SignIn()
        {
            User user = new User();
            return View(user);
        }

        [HttpPost]
        public ActionResult SignIn(User user, string returnUrl)
        {
            var requiredValidation = new string[] { "Email", "Password" };

            foreach (var item in ModelState.Where(k => !requiredValidation.Contains(k.Key)))
                ModelState[item.Key].Errors.Clear();


            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(user.Email, user.Password))
                {
                    FormsAuthentication.SetAuthCookie(user.Email, false);
                    if (!string.IsNullOrWhiteSpace(returnUrl))
                        return Redirect(returnUrl);

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "The email id or password provided is incorrect");
                }
            }
            return View(user);
        }

        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("SignIn");
        }

        public ActionResult ForgotPassword()
        {
            ForgotPassword forgotpassword = new ForgotPassword();
            return View(forgotpassword);
        }
        [HttpPost]
        public ActionResult ForgotPassword(ForgotPassword forgotpassword)
        {
            if (forgotpassword.Email != null)
            {
                var user = _userService.GetAll(k => k.Email == forgotpassword.Email).FirstOrDefault();
                if (user != null)
                {

                    string resetCode = Guid.NewGuid().ToString();
                    user.ResetPasswordCode = resetCode;
                    _userService.Update(user);
                    SendResetEmail(user);
                    return View("PasswordRecovery", user);
                }
                else
                {
                    this.Response.StatusCode = 400;
                    ModelState.AddModelError("", "We couldn't find any account associated with the address :" + forgotpassword.Email.ToString());
                    return View();
                }
            }
            else
            {
                this.Response.StatusCode = 400;
                return View();
            }


        }
        private void SendResetEmail(User user)
        {
            try
            {
                //todo: make it asynchronnious call and template based
                using (System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("sender@gmail.com", user.Email.ToString()))
                {

                    mm.Subject = "Reset Password";
                    string body = "Hello " + user.FirstName.Trim() + ",";
                    body += "<br /><br />Please click the following link to reset your password";
                    body += "<br /><a href = '" + Request.Url.AbsoluteUri.Replace("ForgotPassword", "ResetPassword?ResetPasswordCode=" + user.ResetPasswordCode + "&UserId=" + user.Id.ToString()) + "'>Click here to reset your password.</a>";
                    body += "<br /><br />Thanks";
                    mm.Body = body;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("manojgawas49@gmail.com", "papa2119");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }


            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }


        }

        public ActionResult ResetPassword(string ResetPasswordCode, string UserId)
        {

            ResetPassword reset = new ResetPassword();
            var user = _userService.GetById(Convert.ToInt32(UserId));
            if (user.ResetPasswordCode == ResetPasswordCode)
            {
                reset.Email = user.Email;
                reset.UserId = UserId;
            }
            return View(reset);
        }
        [HttpPost]
        public ActionResult ResetPassword(ResetPassword reset)
        {
            if (ModelState.IsValid)
            {
                var user = _userService.GetById(Convert.ToInt32(reset.UserId));
                user.Password = reset.Password;
                _userService.Update(user);
                return View("VerifiedPassword");
            }
            else
            {
                ModelState.AddModelError("", "Incorrect Password");
                return View("ResetPassword", reset);
            }
        }

    }
}