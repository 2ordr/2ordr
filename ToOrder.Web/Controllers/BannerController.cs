﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Web.App_Start;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Data;

namespace ToOrder.Web.Controllers
{
    public class BannerController : BaseController
    {
        IBannerService _bannerService;


        public BannerController(IBannerService bannerService)
        {
            _bannerService = bannerService;
        }

        // GET: Banner
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ShowCarousel()
        {
            var banners = _bannerService.GetAll(b => b.IsActive && (b.FromDate <= DateTime.Now.Date && b.ToDate >= DateTime.Now.Date)).Take(4);

            return PartialView(banners);
        }
    }
}