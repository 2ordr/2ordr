﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Web.App_Start;
using ToOrder.Core.Services.Interfaces;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using ToOrder.Data;
using ToOrder.Web.ViewModels;

namespace ToOrder.Web.Controllers
{
    public class RestaurantController : BaseController
    {

        IRestaurantService _restaurantService;
        ICuisineService _cuisineService;
        IRestaurantCuisineService _restaurantCuisineService;
        IRatingService _ratingService;

        public RestaurantController(IRestaurantService restaurantService, ICuisineService cuisineService, IRestaurantCuisineService restaurantCuisineservice, IRatingService ratingService)
        {
            _restaurantService = restaurantService;
            _cuisineService = cuisineService;
            _restaurantCuisineService = restaurantCuisineservice;
            _ratingService = ratingService;
        }

        // GET: Restaurant
        public ActionResult Index(string keywords, string cuisineIds)
        {
            RestaurantSearch search = new RestaurantSearch();
            search.Cuisines = _cuisineService.GetAll().ToList();

            if (!string.IsNullOrWhiteSpace(cuisineIds))
                search.SelectedCuisinesId = cuisineIds.Split(',');

            search.Keywords = keywords;
            return View(search);
        }

        public ActionResult SearchRestaurant(string cuisineIds, string budget, string keywords, string place)
        {
            List<int> cuisineList = null;
            List<int> budgetList = null;

            if (!string.IsNullOrWhiteSpace(cuisineIds))
                cuisineList = cuisineIds.Split(',').Select(int.Parse).ToList();

            if (!string.IsNullOrWhiteSpace(budget))
                budgetList = budget.Split(',').Select(int.Parse).ToList();


            var restaurants = _restaurantService.SearchRestaurants(keywords, cuisineList, budgetList, place);
            return PartialView("RestaurantList", restaurants);
        }

        public ActionResult SponsoredRestaurants()
        {
            var restaurants = _restaurantService.GetAll(r => r.IsSponsored).Take(8);
            return PartialView("SponsoredRestaurants", restaurants);
        }
        public ActionResult GoogleMapRestaurants()
        {
            var restaurants = _restaurantService.GetAll();
            return PartialView("GoogleMapRestaurants", restaurants);
        }

        public ActionResult Details(int restaurantId)
        {
            var restaurants = _restaurantService.GetById(restaurantId);
            return PartialView("RestaurantDetails", restaurants);
        }
    }
}