﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Web.App_Start;

namespace ToOrder.Web.Controllers
{
    public class OrderController : BaseController
    {
        IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        // GET: User/Order
        public ActionResult Index(int? page)
        {
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = ToOrderConfigs.DefaultPageSize;
            int total;

            var authenticatedUser = GetAuthenticatedUser();

            var orders = _orderService.GetAll(O => O.UserId == authenticatedUser.Id, pageNumber, pageSize, out total).OrderByDescending(O => O.Date);

            return PartialView(new StaticPagedList<Order>(orders, pageNumber, pageSize, total));
        }

        // GET:User/Order/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        public ActionResult UserRecentRestaurants()
        {
            var authenticatedUser = GetAuthenticatedUser();

            var restaurants = _orderService.GetAll(O => O.UserId == authenticatedUser.Id).OrderByDescending(O => O.Date).Select(i => i.Restaurant).Distinct().Take(4);

            return PartialView("RecentRestaurants", restaurants);
        }


    }
}