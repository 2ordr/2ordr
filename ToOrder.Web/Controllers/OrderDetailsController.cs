﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Web.App_Start;


namespace ToOrder.Web.Controllers
{
    public class OrderDetailsController : BaseController
    {
        IOrderDetailService _orderDetailService;

        public OrderDetailsController(IOrderDetailService orderDetailService)
        {
            _orderDetailService = orderDetailService;
        }

        // GET: OrderDetails
        public ActionResult Index(int orderId)
        {
            var orderDetails = _orderDetailService.GetAll(o => o.OrderId == orderId);

            return PartialView(orderDetails.ToList());
        }

        // GET: OrderDetail/Details/5
        public ActionResult Details(int orderId)
        {
            return View();
        }

        // GET: OrderDetail/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: OrderDetail/Create
        [HttpPost]
        public ActionResult Create(OrderDetails orderDetails)
        {
            return View();
        }

        // GET: OrderDetail/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: OrderDetail/Edit/5
        [HttpPost]
        public ActionResult Edit(OrderDetails orderDetails)
        {
            return View();
        }

        // GET: OrderDetail/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: OrderDetail/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            return View();
        }
    }
}