﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Web.Helpers;

namespace ToOrder.Web.Controllers
{
    public class LanguageController : BaseController
    {
       
        // [ChildActionOnly]
        public ActionResult LanguageSelector()
        {

            Language language = new Language();
            language.Culture = cultureName;
            return PartialView("LanguageSelector", language);
        }


        public ActionResult SetCulture(string culture)
        {
            // Validate input
            culture = CultureHelper.GetImplementedCulture(culture);  //todo: pass supported cultures from Language

            // Save culture in a cookie
            HttpCookie cookie = Request.Cookies["_culture"];
            if (cookie != null)
                cookie.Value = culture;   // update cookie value
            else
            {
                cookie = new HttpCookie("_culture");
                cookie.Value = culture;
                cookie.Expires = DateTime.Now.AddYears(1);
            }

            Response.Cookies.Add(cookie);
            return Redirect(Request.UrlReferrer.ToString());
        }
    }
}