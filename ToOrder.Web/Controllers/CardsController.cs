﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Web.ViewModels;
namespace ToOrder.Web.Controllers
{
    public class CardsController : BaseController
    {
        IUserCardService _userCardService;

        public CardsController(IUserCardService userCardService)
        {
            _userCardService = userCardService;
        }

        // GET: Cards
        public ActionResult Index()
        {
            var user = GetAuthenticatedUser();
            var userCards = _userCardService.GetAll(k => k.UserId == user.Id);
            return PartialView(userCards);
        }

        public ActionResult Create()
        {
            UserCard card = new UserCard();
            ViewBag.Result = "Message";
            return PartialView(card);
        }

        // POST: User/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserCard cards)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = GetAuthenticatedUser();
                    cards.UserId = user.Id;
                    cards.CardNumber = cards.CardNumber.Replace('-', ' ').Replace(" ", String.Empty);
                    if (cards.CardNumber.Length < 12)
                    {
                        ModelState.AddModelError(string.Empty, "Please enter valid card number");

                        return PartialView(cards);
                    }
                    _userCardService.AddNew(cards);
                    return RedirectToAction("Index");

                }
                else
                {
                    return PartialView(cards);
                }


            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return PartialView(cards);
            }
        }

        public ActionResult Delete(int id)
        {
            UserCard card = _userCardService.GetById(id);
            return PartialView("Delete", card);
        }
        [HttpPost]
        public ActionResult Delete(UserCard card)
        {
            try
            {
                _userCardService.Delete(card);
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }
    }
}