﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using ToOrder.Localization;

namespace ToOrder.Web.ViewModels
{
    public class UserSignUpVM
    {

        [Display(Name = "Email", ResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "EmailRequiredError")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

         
        [Display(Name = "Password", ResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "PasswordRequiredError")]
        [MinLength(5, ErrorMessage = "Minimum 5 characters")]
        [DataType(DataType.Password)]
        public string Password { get; set; }



         
        [MinLength(5)]
        [DataType(DataType.Password)]
        [NotMapped]
        [Compare("Password", ErrorMessage = "Password and Confirmation Password doesn't match")]
        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "ConfirmPasswordRequiredError")]
        public string ConfirmPassword { get; set; }


         
        [Display(Name = "FirstName", ResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "FirstNameRequiredError")]
        public string FirstName { get; set; }
        
        [Display(Name = "LastName", ResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "LastNameRequiredError")]
        public string LastName { get; set; }


        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Mobile", ResourceType = typeof(Resources))]
        public string Mobile { get; set; }


    }
}