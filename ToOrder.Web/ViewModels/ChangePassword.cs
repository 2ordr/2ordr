﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using ToOrder.Localization;

namespace ToOrder.Web.ViewModels
{
    public class ChangePassword
    {
        [Required]
        [Display(Name = "CurrentPassword", ResourceType = typeof(Resources))]
        [MinLength(5, ErrorMessage = "Minimum 5 characters")]
        [DataType(DataType.Password)]
        public string CurrentPassword { get; set; }

        [Display(Name = "Password", ResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "PasswordRequiredError")]
        [MinLength(5, ErrorMessage = "Minimum 5 characters")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        
         
        [MinLength(5)]
        [DataType(DataType.Password)]
        [NotMapped]
        [Display(Name = "ConfirmPassword", ResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "ConfirmPasswordRequiredError")]
        [Compare("Password", ErrorMessage = "Password and Confirmation Password doesn't match")]
        public string ConfirmPassword { get; set; }
        
    }
}