﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToOrder.Core.Entities;

namespace ToOrder.Web.ViewModels
{
    public class RestaurantMenu
    {
        public IEnumerable<Category> Categories { get; set; }

        public IEnumerable<MenuItem> MenuItems { get; set; }
    }
}