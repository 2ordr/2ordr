﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Localization;

namespace ToOrder.Web.ViewModels
{
    public class ForgotPassword
    {
        [Display(Name = "Email", ResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "EmailRequiredError")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}
