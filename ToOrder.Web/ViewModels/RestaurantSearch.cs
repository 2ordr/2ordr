﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToOrder.Core.Entities;

namespace ToOrder.Web.ViewModels
{
    public class RestaurantSearch
    {
        public IEnumerable<Cuisine> Cuisines { get; set; }

        public string Keywords { get; set; }

        public string[]  SelectedCuisinesId { get; set; }

    }
}