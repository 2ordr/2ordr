﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Localization;

namespace ToOrder.Web.ViewModels
{
    public class ResetPassword
    {
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Password", ResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "PasswordRequiredError")]
        [MinLength(5, ErrorMessage = "Minimum 5 characters")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "ConfirmPassword", ResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "ConfirmPasswordRequiredError")]
        [MinLength(5)]
        [DataType(DataType.Password)]
        [NotMapped]
        [Compare("Password", ErrorMessage = "Password and Confirmation Password doesn't match")]
        public string ConfirmPassword { get; set; }
        public string UserId { get; set; }
    }
}
