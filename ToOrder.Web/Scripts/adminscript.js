﻿
$(document).ready(function () {
    $("li > a").click(function () {
        var id = $(this).attr("id");

        $('#' + selectedolditem).not(this).removeClass("activeBgColor");
        $('#' + id).addClass("activeBgColor");

        localStorage.setItem("selectedolditem", id);
        selectedolditem = localStorage.getItem('selectedolditem');

    });

    var selectedolditem = localStorage.getItem('selectedolditem');

    if (selectedolditem != null) {
        $('#' + selectedolditem).addClass("activeBgColor");
        $('#' + selectedolditem).parents('ul').css('display', 'block');
        $('#' + selectedolditem).parents('li:first').css('background-color', '#fcb03b');
    }

});