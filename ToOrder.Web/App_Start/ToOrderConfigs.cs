﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ToOrder.Web.App_Start
{
    public static class ToOrderConfigs
    {
        public static int DefaultPageSize
        {
            get
            {
                int pageSize = 50;
                int.TryParse(ConfigurationManager.AppSettings.Get("PageSize"), out pageSize);
                return pageSize;
            }
        }
    }
}