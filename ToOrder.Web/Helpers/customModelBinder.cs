﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ToOrder.Web.Helpers
{
    public class CustomModelBinder : DefaultModelBinder
    {
        public CustomModelBinder()
            : base()
        {
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {

            object result = null;

            if (bindingContext.ModelType == typeof(double))
            {

                string modelName = bindingContext.ModelName;
                string attemptedValue = bindingContext.ValueProvider.GetValue(modelName).AttemptedValue;

                string wantedSeperator = NumberFormatInfo.CurrentInfo.NumberDecimalSeparator;
                string alternateSeperator = (wantedSeperator == "," ? "." : ",");

                if (attemptedValue.IndexOf(wantedSeperator) == -1
                  && attemptedValue.IndexOf(alternateSeperator) != -1)
                {
                    attemptedValue = attemptedValue.Replace(alternateSeperator, wantedSeperator);
                }

                try
                {
                    if (bindingContext.ModelMetadata.IsNullableValueType
                    && string.IsNullOrWhiteSpace(attemptedValue))
                    {
                        return null;
                    }

                    result = double.Parse(attemptedValue, NumberStyles.Any);
                }
                catch (FormatException e)
                {
                    bindingContext.ModelState.AddModelError(modelName, e);
                }
            }
            else
            {
                if (bindingContext.ModelType == typeof(decimal))
                {

                    string modelName = bindingContext.ModelName;
                    string attemptedValue = bindingContext.ValueProvider.GetValue(modelName).AttemptedValue;

                    string wantedSeperator = NumberFormatInfo.CurrentInfo.NumberDecimalSeparator;
                    string alternateSeperator = (wantedSeperator == "," ? "." : ",");

                    if (attemptedValue.IndexOf(wantedSeperator) == -1
                      && attemptedValue.IndexOf(alternateSeperator) != -1)
                    {
                        attemptedValue = attemptedValue.Replace(alternateSeperator, wantedSeperator);
                    }

                    try
                    {
                        if (bindingContext.ModelMetadata.IsNullableValueType
                        && string.IsNullOrWhiteSpace(attemptedValue))
                        {
                            return null;
                        }

                        result = decimal.Parse(attemptedValue, NumberStyles.Any);
                    }
                    catch (FormatException e)
                    {
                        bindingContext.ModelState.AddModelError(modelName, e);
                    }
                }
                else
                {
                    result = base.BindModel(controllerContext, bindingContext);
                }

            }
            return result;

        }
    }
}

