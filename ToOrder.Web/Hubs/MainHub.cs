﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace ToOrder.Web.Hubs
{
    public class MainHub : Hub
    {
        public void Test()
        {
            Clients.User(@"rest@certigoa.com").testMessage("Sent from MainHub");
        }

        public void OrderNotification()
        {

        }
    }
}