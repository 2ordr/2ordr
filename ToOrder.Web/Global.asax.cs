﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ToOrder.Data;
using ToOrder.Web.Helpers;

namespace ToOrder.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Database.SetInitializer<ToOrderDbContext>(new DatabaseInitializer());

            ModelBinders.Binders.Add(typeof(double), new CustomModelBinder());
            ModelBinders.Binders.Add(typeof(double?), new CustomModelBinder());
            ModelBinders.Binders.Add(typeof(decimal), new CustomModelBinder());
            ModelBinders.Binders.Add(typeof(decimal?), new CustomModelBinder());

        }
    }
}
