﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Services.Interfaces;
using PagedList;
using ToOrder.Core.Entities;
using ToOrder.Web.App_Start;
using System.Globalization;
using System.IO;
using ToOrder.Web.Controllers;

namespace ToOrder.Web.Areas.Admin.Controllers
{
    public class RestaurantController : BaseController
    {
        IRestaurantService _restaurantService;
        public RestaurantController(IRestaurantService restaurantService)
        {
            _restaurantService = restaurantService;
        }


        


        // GET: Admin/Restaurant
        public ActionResult Index(int? page)
        {
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = ToOrderConfigs.DefaultPageSize;

            int total;
            var restaurants = _restaurantService.GetAll(pageNumber, pageSize, out total);

            //using StaticPagedList for better performance
            return View(new StaticPagedList<Restaurant>(restaurants, pageNumber, pageSize, total));
        }



        public ActionResult Search(string restaurant, string cuisine, int? page)
        {
            ViewBag.resturantName = restaurant;
            ViewBag.cuisine = cuisine;

            int pageNumber = page != null ? page.Value : 1;
            int pageSize = ToOrderConfigs.DefaultPageSize;

            int total;


            var restaurants = _restaurantService.GetAll(r => restaurant == null || r.Name.IndexOf(restaurant, StringComparison.InvariantCultureIgnoreCase) >= 0, pageNumber, pageSize, out total);
            return View("Index", new StaticPagedList<Restaurant>(restaurants, pageNumber, pageSize, total));
        }

        // GET: Admin/Restaurant/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/Restaurant/Create
        public ActionResult Create()
        {
            var restaurant = new Restaurant();

            return View(restaurant);
        }

        // POST: Admin/Restaurant/Create
        [HttpPost]
        public ActionResult Create(Restaurant restaurant, HttpPostedFileBase uploadFile)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string folderPath = @"\Pictures\Restaurant";
                    string folderPathOnServer = Server.MapPath(folderPath);
                    if (!Directory.Exists(folderPathOnServer))
                    {
                        Directory.CreateDirectory(folderPathOnServer);
                    }
                    if (uploadFile != null)
                    {
                        if (uploadFile.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".png" };
                            var filename = Path.GetFileName(uploadFile.FileName);
                            var ext = Path.GetExtension(uploadFile.FileName);

                            if (allowedExtensions.Contains(ext))
                            {
                                restaurant.PictureFileName = filename;
                                restaurant.CreationDate = System.DateTime.Now;
                                _restaurantService.AddNew(restaurant);

                                int resID = restaurant.Id;
                                string myPicture = resID + "_" + filename;
                                var path = Path.Combine(folderPathOnServer, myPicture);
                                uploadFile.SaveAs(path);

                                return RedirectToAction("Index");
                            }
                            else
                            {
                                ModelState.AddModelError("", "Image Extension is not Valid");
                                return View(restaurant);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "Please Select Proper Image ");
                            return View(restaurant);
                        }
                    }
                    else
                    {
                        restaurant.CreationDate = System.DateTime.Now;
                        _restaurantService.AddNew(restaurant);
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    return   View(restaurant);
                }
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                if (ex is InvalidOperationException)
                    ModelState.AddModelError("", "Please correct following errors:");
                else
                    ModelState.AddModelError("", ex.Message);
                return RedirectToAction("Create");
            }
        }

        // GET: Admin/Restaurant/Edit/5
        public ActionResult Edit(int id)
        {
            var restaurant = _restaurantService.GetById(id);
            return View(restaurant);
        }

        // POST: Admin/Restaurant/Edit/5
        [HttpPost]
        public ActionResult Edit(Restaurant restaurant, HttpPostedFileBase uploadFile)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string folderPath = @"\Pictures\Restaurant";
                    string folderPathOnServer = Server.MapPath(folderPath);
                    if (!Directory.Exists(folderPathOnServer))
                    {
                        Directory.CreateDirectory(folderPathOnServer);
                    }
                    if (uploadFile != null)
                    {
                        if (uploadFile.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".png" };
                            var filename = Path.GetFileName(uploadFile.FileName);
                            var ext = Path.GetExtension(uploadFile.FileName);

                            if (allowedExtensions.Contains(ext))
                            {
                                restaurant.PictureFileName = filename;
                                _restaurantService.Update(restaurant);

                                int resID = restaurant.Id;
                                string myPicture = resID + "_" + filename;
                                var path = Path.Combine(folderPathOnServer, myPicture);
                                uploadFile.SaveAs(path);

                                return RedirectToAction("Index");
                            }
                            else
                            {
                                ModelState.AddModelError("", "Image Extension is not Valid");
                                return View(restaurant);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "Please Select Proper Image ");
                            return View(restaurant);
                        }
                    }
                    else
                    {
                        _restaurantService.Update(restaurant);
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    return RedirectToAction("Edit", new { restaurantId = restaurant.Id });
                }
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                if (ex is InvalidOperationException)
                    ModelState.AddModelError("", "Please correct following errors:");
                else
                    ModelState.AddModelError("", ex.Message);
                return RedirectToAction("Edit", new { restaurantId = restaurant.Id });
            }
        }

        // GET: Admin/Restaurant/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/Restaurant/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Dashboard()
        {
            return View();
        }
    }
}
