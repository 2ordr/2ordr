﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Services.Interfaces;
using PagedList;
using ToOrder.Core.Entities;
using ToOrder.Web.App_Start;
using System.Globalization;
using System.Text;
using ToOrder.Web.Controllers;

namespace ToOrder.Web.Areas.Admin.Controllers
{
    public class CuisineController : BaseController
    {
        ICuisineService _cuisineService;

        public CuisineController(ICuisineService cuisineService)
        {
            _cuisineService = cuisineService;
        }
        // GET: Admin/Cuisine
        public ActionResult Index(int? page)
        {
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = ToOrderConfigs.DefaultPageSize;

            int total;
            var cuisines = _cuisineService.GetAll(pageNumber, pageSize, out total);

            //using StaticPagedList for better performance
            return View(new StaticPagedList<Cuisine>(cuisines, pageNumber, pageSize, total));
        }
        public ActionResult Search(string cuisine, int? page)
        {
            ViewBag.cuisine = cuisine;

            int pageNumber = page != null ? page.Value : 1;
            int pageSize = ToOrderConfigs.DefaultPageSize;

            int total;


            var cuisines = _cuisineService.GetAll(r => cuisine == null || r.Name.IndexOf(cuisine, StringComparison.InvariantCultureIgnoreCase) >= 0, pageNumber, pageSize, out total);
            return View("Index", new StaticPagedList<Cuisine>(cuisines, pageNumber, pageSize, total));
        }

        // GET: Admin/cuisine/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/cuisine/Create
        public ActionResult Create()
        {
            Cuisine cuisine = new Cuisine();
            return PartialView("Create", cuisine); 
        }

        // POST: Admin/cuisine/Create
        [HttpPost]
        public ActionResult Create(Cuisine model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var isCuisineExists = _cuisineService.GetAll(c=>c.Name.ToLower()==model.Name.ToLower()).Count();
                    if (isCuisineExists == 0)
                    {
                        model.CreationDate = System.DateTime.Now;
                        _cuisineService.AddNew(model);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        this.Response.StatusCode = 400;
                        ModelState.AddModelError("","Cuisine already exists");
                        return PartialView("Create", model);
                    }
                }

                this.Response.StatusCode = 400;
                return PartialView("Create", model);


            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                ModelState.AddModelError("", ex.Message);
                return PartialView("Create", model);
            }
        }
        // GET: Admin/cuisine/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Admin/cuisine/Edit/5
        [HttpPost]
        public ActionResult Edit(Cuisine cuisine)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // TODO: Add update logic here

                    cuisine.Name = cuisine.Name.Replace('\n', ' ').Trim();
                    int value = _cuisineService.GetAll(s => s.Name.ToLower() == cuisine.Name.ToLower() & s.Id != cuisine.Id).Count();
                    if (value > 0)
                    {

                        ModelState.AddModelError(string.Empty, "Cuisine name exists");
                        this.Response.StatusCode = 400;
                        return Json(new { success = false, errors = GetModelStateErrors(ModelState) }, JsonRequestBehavior.AllowGet);
                    }
                    _cuisineService.Update(cuisine);
                    return Json(new { success = true, msg = cuisine.Id }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var errorModel =
                    from x in ModelState.Keys
                    where ModelState[x].Errors.Count > 0
                    select new
                    {
                        key = x,
                        errors = ModelState[x].Errors.
                                               Select(y => y.ErrorMessage).
                                               ToArray()
                    };

                    this.Response.StatusCode = 400;
                    ModelState.AddModelError(string.Empty, "Please correct the errors");
                    return Json(new { success = false, errors = GetModelStateErrors(ModelState) }, JsonRequestBehavior.AllowGet);

                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View();
            }
        }

        public List<string> GetModelStateErrors(ModelStateDictionary ModelState)
        {
            List<string> errorMessages = new List<string>();

            var validationErrors = ModelState.Values.Select(x => x.Errors);
            validationErrors.ToList().ForEach(ve =>
            {
                var errorStrings = ve.Select(x => x.ErrorMessage);
                errorStrings.ToList().ForEach(em =>
                {
                    errorMessages.Add(em);
                });
            });

            return errorMessages;
        }
        // GET: Admin/cuisine/Delete/5
        public ActionResult Delete(int id)
        {
            Cuisine cuisine = _cuisineService.GetById(id);
            return PartialView("Delete", cuisine);
        }

        // POST: Admin/cuisine/Delete/5
        [HttpPost]
        public ActionResult Delete(Cuisine cuisine)
        {
            try
            {
                // TODO: Add delete logic here
                _cuisineService.Delete(cuisine);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}