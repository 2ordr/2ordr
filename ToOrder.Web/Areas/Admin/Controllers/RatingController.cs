﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Web.App_Start;
using ToOrder.Web.Controllers;

namespace ToOrder.Web.Areas.Admin.Controllers
{
    public class RatingController : BaseController
    {
        IRatingService _ratingService;

        public RatingController(IRatingService ratingService)
        {
            _ratingService = ratingService;
        }

        // GET: Admin/Rating
        public ActionResult Index(int? page)
        {
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = ToOrderConfigs.DefaultPageSize;

            int total;
            var ratings = _ratingService.GetAll(pageNumber, pageSize, out total);

            return View(new StaticPagedList<Rating>(ratings, pageNumber, pageSize, total));
        }

        public ActionResult Search(string restaurant, int? page)
        {
            ViewBag.restaurant = restaurant;

            int pageNumber = page != null ? page.Value : 1;
            int pageSize = ToOrderConfigs.DefaultPageSize;

            int total;


            var ratings = _ratingService.GetAll(r => restaurant == null || r.Restaurant.Name.IndexOf(restaurant, StringComparison.InvariantCultureIgnoreCase) >= 0, pageNumber, pageSize, out total);
            return View("Index", new StaticPagedList<Rating>(ratings, pageNumber, pageSize, total));
        }

        // GET: Admin/rating/Delete/5
        public ActionResult Delete(int id)
        {
            Rating rating = _ratingService.GetById(id);
            return PartialView("Delete", rating);
        }

        // POST: Admin/rating/Delete/5
        [HttpPost]
        public ActionResult Delete(Rating rating)
        {
            try
            {
                _ratingService.Delete(rating);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                if (ex is InvalidOperationException)
                    ModelState.AddModelError("", "Please correct following errors:");
                else
                    ModelState.AddModelError("", ex.Message);
                return PartialView("Delete", rating);
            }
        }

        // GET: Admin/rating/Update/5
        public ActionResult Update(int id)
        {
            try
            {
                Rating rating = _ratingService.GetById(id);
                rating.Blocked = true;

                _ratingService.Update(rating);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                if (ex is InvalidOperationException)
                    ModelState.AddModelError("", "Please correct following errors:");
                else
                    ModelState.AddModelError("", ex.Message);
                return RedirectToAction("Index");
            }
        }
    }
}