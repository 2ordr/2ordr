﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Services.Interfaces;
using PagedList;
using ToOrder.Core.Entities;
using ToOrder.Web.App_Start;
using System.IO;
using ToOrder.Web.Controllers;
using ToOrder.Localization;

namespace ToOrder.Web.Areas.Admin.Controllers
{
    public class BannerController : BaseController
    {
        IBannerService _bannerService;
        IRestaurantService _restaurantService;

        public BannerController(IBannerService bannerService, IRestaurantService restaurantService)
        {
            _bannerService = bannerService;
            _restaurantService = restaurantService;
        }


        // GET: Admin/Banner
        public ActionResult Index(int? page)
        {
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = ToOrderConfigs.DefaultPageSize;

            int total;
            var banners = _bannerService.GetAll(pageNumber, pageSize, out total);
            ViewBag.restaurant = _restaurantService.GetAll().ToList();

            //using StaticPagedList for better performance
            return View(new StaticPagedList<Banner>(banners, pageNumber, pageSize, total));
        }

        public ActionResult Search(int? restaurantId, DateTime? fromDate, DateTime? toDate, int? page)
        {
            ViewBag.restaurant = _restaurantService.GetAll().ToList();
            ViewBag.restaurants = restaurantId;

            int pageNumber = page != null ? page.Value : 1;
            int pageSize = ToOrderConfigs.DefaultPageSize;

            int total;

            var banners = _bannerService.GetAll(b => (restaurantId == null || b.RestaurantId == restaurantId) && (fromDate == null || b.FromDate >= fromDate) && (toDate == null || b.ToDate <= toDate), pageNumber, pageSize, out total);
            return View("Index", new StaticPagedList<Banner>(banners, pageNumber, pageSize, total));

        }


        // GET: Admin/Banner/Create
        public ActionResult Create()
        {
            var banner = new Banner();
            ViewBag.restaurant = _restaurantService.GetAll().ToList();

            return View(banner);
        }

        // POST: Admin/Banner/Create
        [HttpPost]
        public ActionResult Create(Banner banner, HttpPostedFileBase uploadFile)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string folderPath = @"\Pictures\Banner";
                    string folderPathOnServer = Server.MapPath(folderPath);

                    if (!Directory.Exists(folderPathOnServer))
                    {
                        Directory.CreateDirectory(folderPathOnServer);
                    }
                    if (uploadFile != null)
                    {
                        if (uploadFile.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".png" };
                            var filename = Path.GetFileName(uploadFile.FileName);
                            var ext = Path.GetExtension(uploadFile.FileName);

                            if (allowedExtensions.Contains(ext))
                            {
                                banner.FileName = filename;
                                _bannerService.AddNew(banner);

                                int bannerID = banner.Id;
                                string mybanner = bannerID + "_" + filename;
                                var path = Path.Combine(folderPathOnServer, mybanner);
                                uploadFile.SaveAs(path);

                                return RedirectToAction("Index");
                            }
                            else
                            {
                                ModelState.AddModelError("", Resources.ImageExtensionNotValid);
                                ViewBag.restaurant = _restaurantService.GetAll().ToList();
                                return View(banner);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", Resources.PleaseSelectProperSizeImage);
                            ViewBag.restaurant = _restaurantService.GetAll().ToList();
                            return View(banner);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", Resources.PleaseSelectProperSizeImage);
                        ViewBag.restaurant = _restaurantService.GetAll().ToList();
                        return View(banner);
                    }
                }
                else
                {
                    ViewBag.restaurant = _restaurantService.GetAll().ToList();
                    return View(banner);

                }
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                if (ex is InvalidOperationException)
                    ModelState.AddModelError("", Resources.PleaseCorrectfollowingErrors);
                else
                    ModelState.AddModelError("", ex.Message);
                return RedirectToAction("Create");
            }
        }

        // GET: Admin/Banner/Delete/5
        public ActionResult Delete(int id)
        {
            Banner banner = _bannerService.GetById(id);
            return PartialView("Delete", banner);
        }

        // POST: Admin/Banner/Delete/5
        [HttpPost]
        public ActionResult Delete(Banner banner)
        {
            try
            {
                _bannerService.Delete(banner);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                if (ex is InvalidOperationException)
                    ModelState.AddModelError("", Resources.PleaseCorrectfollowingErrors);
                else
                    ModelState.AddModelError("", ex.Message);
                return PartialView("Delete", banner);
            }
        }
    }
}