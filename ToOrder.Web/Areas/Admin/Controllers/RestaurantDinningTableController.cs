﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Web.Controllers;

namespace ToOrder.Web.Areas.Admin.Controllers
{
    public class RestaurantDinningTableController : BaseController
    {
        IDinningTableService _dinningTableService;
        IMenuCardService _menuCardService;

        public RestaurantDinningTableController(
            IDinningTableService dinningTableService
           , IMenuCardService menuCardService)
        {
            _dinningTableService = dinningTableService;
            _menuCardService = menuCardService;
        }

        // GET: Admin/RestaurantDinningTable
        public ActionResult Index(int restaurantId)
        {
            var restaurantdinningTable = _dinningTableService.GetAll(c => c.MenuCard.RestaurantId == restaurantId);
            ViewBag.RestaurantId = restaurantId;
            return PartialView(restaurantdinningTable);
        }

        // GET: Admin/RestaurantDinningTable/Details/1
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/RestaurantDinningTable/Create
        public ActionResult Create(int restaurantId)
        {
            DinningTable dinningTable = new DinningTable { };

            ViewBag.MenuCardId = _menuCardService.GetAll().ToList().Where(r => r.RestaurantId == restaurantId).Select(m => new SelectListItem { Value = m.Id.ToString(), Text = m.Name }).ToList();
            return PartialView("AddEditPartial", dinningTable);
        }

        // POST: Admin/RestaurantDinningTable/Create
        [HttpPost]
        public ActionResult Create(DinningTable dinningTable)
        {
            try
            {
                _dinningTableService.AddNew(dinningTable);

                return RedirectToAction("Index", new { restaurantId = dinningTable.MenuCard.RestaurantId });
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                if (ex is InvalidOperationException)
                    ModelState.AddModelError("", "Please correct following errors:" + Environment.NewLine + ex.Message);
                else
                    ModelState.AddModelError("", ex.Message);

                ViewBag.MenuCardId = _menuCardService.GetAll().ToList().Where(r => r.RestaurantId == dinningTable.MenuCard.RestaurantId).Select(m => new SelectListItem { Value = m.Id.ToString(), Text = m.Name }).ToList();
                return PartialView("AddEditPartial", dinningTable);
            }
        }

        // GET: Admin/RestaurantDinningTable/Edit/5
        public ActionResult Edit(int id, int restaurantId)
        {
            DinningTable dinningTables = _dinningTableService.GetById(id);

            ViewBag.dinningTableList = _menuCardService.GetAll().ToList().Where(r => r.RestaurantId == restaurantId).Select(m => new SelectListItem { Value = m.Id.ToString(), Text = m.Name }).ToList();
            return PartialView("AddEditPartial", dinningTables);
        }

        // POST: Admin/RestaurantDinningTable/Edit/5
        [HttpPost]
        public ActionResult Edit(DinningTable dinningTable, int restaurantId)
        {
            try
            {
                _dinningTableService.Update(dinningTable);

                return RedirectToAction("Index", new { restaurantId });
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                if (ex is InvalidOperationException)
                    ModelState.AddModelError("", "Please correct following errors:");
                else
                    ModelState.AddModelError("", ex.Message);

                return PartialView("AddEditPartial", dinningTable);
            }
        }

        // GET: Admin/RestaurantDinningTable/Delete/5
        public ActionResult Delete(int id)
        {
            DinningTable dinningTable = _dinningTableService.GetById(id);
            return PartialView("Delete", dinningTable);
        }

        // POST: Admin/RestaurantDinningTable/Delete/5
        [HttpPost]
        public ActionResult Delete(DinningTable dinningTable)
        {
            try
            {
                _dinningTableService.Delete(dinningTable);

                return RedirectToAction("Index", new { restaurantId = dinningTable.MenuCard.RestaurantId });
            }
            catch
            {
                return View();
            }
        }
    }
}