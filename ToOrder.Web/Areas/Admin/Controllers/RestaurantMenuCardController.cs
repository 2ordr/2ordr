﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Web.Controllers;

namespace ToOrder.Web.Areas.Admin.Controllers
{
    public class RestaurantMenuCardController : BaseController
    {
        IMenuCardService _menucardService;
        IMenuItemService _menuItemService;

        public RestaurantMenuCardController(
            IMenuCardService menucardService,IMenuItemService menuItemService)
        {
            _menucardService = menucardService;
            _menuItemService = menuItemService;
        }

        // GET: Admin/RestaurantMenuCard
        public ActionResult Index(int restaurantId)
        {
            var restaurantMenuCard = _menucardService.GetAll(c => c.RestaurantId == restaurantId);
            ViewBag.RestaurantId = restaurantId;
            return PartialView(restaurantMenuCard);
        }
        // GET: Admin/RestaurantMenuCard/Details/5
        public ActionResult Details(int id)
        {

            IEnumerable<MenuItem> menuItem = _menuItemService.GetAll(s => s.MenuCardId == id).ToList();
            ViewBag.MenuCardName = _menucardService.GetById(id).Name;
            ViewBag.RestaurantName = _menucardService.GetById(id).Restaurant.Name;
            ViewBag.RestaurantId = _menucardService.GetById(id).RestaurantId;
            ViewBag.MenuCardId = id;
            return View(menuItem);
        }
        // GET: Admin/RestaurantMenuCard/Create
        public ActionResult Create(int restaurantId)
        {
            MenuCard restaurantMenuCard = new MenuCard
            {
                RestaurantId = restaurantId
            };
            return PartialView("AddEditPartial", restaurantMenuCard);
        }

        // POST: Admin/RestaurantMenuCard/Create
        [HttpPost]
        public ActionResult Create(MenuCard restaurantMenuCard)
        {
            try
            {
                if (restaurantMenuCard.IsDefault == true)
                {
                    var defaultMenuCard = _menucardService.GetAll(c => c.IsDefault == true && c.RestaurantId == restaurantMenuCard.RestaurantId);
                    if (defaultMenuCard.Count() > 0)
                    {
                        this.Response.StatusCode = 400;
                        ModelState.AddModelError("", "Default already set to another menu card");
                        return PartialView("AddEditPartial", restaurantMenuCard);
                    }
                }
                _menucardService.AddNew(restaurantMenuCard);
                return RedirectToAction("Index", new { restaurantId = restaurantMenuCard.RestaurantId });
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                if (ex is InvalidOperationException)
                    ModelState.AddModelError("", "Please correct following errors:" + Environment.NewLine + ex.Message);
                else
                    ModelState.AddModelError("", ex.Message);

                return PartialView("AddEditPartial", restaurantMenuCard);
            }
        }
        // GET: Admin/RestaurantMenuCard/Edit/5
        public ActionResult Edit(int id)
        {
            MenuCard restaurantMenuCard = _menucardService.GetById(id);
            return PartialView("AddEditPartial", restaurantMenuCard);
        }

        // POST: Admin/RestaurantMenuCard/Edit/5
        [HttpPost]
        public ActionResult Edit(MenuCard restaurantMenuCard)
        {
            try
            {
                if (restaurantMenuCard.IsDefault == true)
                {
                    var defaultMenuCard = _menucardService.GetAll(c => c.Id != restaurantMenuCard.Id && c.IsDefault == true && c.RestaurantId == restaurantMenuCard.RestaurantId).ToList();
                    foreach (MenuCard menu in defaultMenuCard)
                    {
                        menu.IsDefault = false;
                        _menucardService.Update(menu);
                    }
                }
                _menucardService.Update(restaurantMenuCard);

                return RedirectToAction("Index", new { restaurantId = restaurantMenuCard.RestaurantId });
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                if (ex is InvalidOperationException)
                    ModelState.AddModelError("", "Please correct following errors:");
                else
                    ModelState.AddModelError("", ex.Message);
                return PartialView("AddEditPartial", restaurantMenuCard);
            }
        }
        // GET: Admin/RestaurantMenuCard/Delete/5
        public ActionResult Delete(int id)
        {
            MenuCard restaurantMenuCard = _menucardService.GetById(id);
            var isMenuItemsLinked = _menuItemService.GetAll(m => m.MenuCardId == id).Count();
            if (isMenuItemsLinked > 0)
            {
                return PartialView("MenuItemsLinked", restaurantMenuCard);
            }
            else
            {

                return PartialView("Delete", restaurantMenuCard);
            }
        }

        // POST: Admin/RestaurantMenuCard/Delete/5
        [HttpPost]
        public ActionResult Delete(MenuCard restaurantMenuCard)
        {
            try
            {
                // TODO: Add delete logic here
                _menucardService.Delete(restaurantMenuCard);
                return RedirectToAction("Index", new { restaurantId = restaurantMenuCard.RestaurantId });
            }
            catch
            {
                return View();
            }
        }

    }
}