﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Web.App_Start;
using ToOrder.Web.Controllers;

namespace ToOrder.Web.Areas.Admin.Controllers
{
    public class OrderController : BaseController
    {
        IOrderService _orderService;
        IRestaurantService _restaurantService;

        public OrderController(IOrderService orderService, IRestaurantService restaurantService)
        {
            _orderService = orderService;
            _restaurantService = restaurantService;
        }

        // GET: Admin/Order
        public ActionResult Index(int? page)
        {
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = ToOrderConfigs.DefaultPageSize;

            int total;
            var orders = _orderService.GetAll(pageNumber, pageSize, out total);
            ViewBag.restaurant = _restaurantService.GetAll().ToList();

            return View(new StaticPagedList<Order>(orders, pageNumber, pageSize, total));
        }

        public ActionResult Search(int? restaurantId, decimal? amount, DateTime? fromDate, DateTime? toDate, int? page)
        {
            ViewBag.restaurant = _restaurantService.GetAll().ToList();
            ViewBag.restaurants = restaurantId;

            int pageNumber = page != null ? page.Value : 1;
            int pageSize = ToOrderConfigs.DefaultPageSize;

            int total;

            var orders = _orderService.GetAll(r => (restaurantId == null || r.RestaurantId == restaurantId) && (amount == null || r.NetAmount >= amount) && (fromDate == null || r.Date >= fromDate) && (toDate == null || r.Date <= toDate), pageNumber, pageSize, out total);
            return View("Index", new StaticPagedList<Order>(orders, pageNumber, pageSize, total));
        }
        public ActionResult OrdersToday(int? page)
        {
            DateTime NewDateValue = System.DateTime.Now.Date;
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = 10;
            int total;
            var orders = _orderService.GetAll(c => c.Date.Date == NewDateValue, pageNumber, pageSize, out total);
            return View(new StaticPagedList<Order>(orders, pageNumber, pageSize, total));
        }
        public ActionResult SearchTodaysOrders(string order, int? page)
        {
            DateTime NewDateValue = System.DateTime.Now.Date;
            ViewBag.orders = order;
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = 10;

            int total;
            var orders = _orderService.GetAll(c =>  c.Date.Date == NewDateValue, pageNumber, pageSize, out total);
            return PartialView("OrdersToday", new StaticPagedList<Order>(orders, pageNumber, pageSize, total));
        }
        public ActionResult RestaurantsToday(int? page)
        {
            DateTime NewDateValue = System.DateTime.Now.Date;
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = 10;
            int total;
            var restaurants = _restaurantService.GetAll(c => c.CreationDate.Date == NewDateValue, pageNumber, pageSize, out total);
            return View(new StaticPagedList<Restaurant>(restaurants, pageNumber, pageSize, total));
        }
        public ActionResult SearchRestaurantTodays(string restaurant, int? page)
        {
            DateTime NewDateValue = System.DateTime.Now.Date;
            ViewBag.restaurant = restaurant;
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = 10;

            int total;
            var restaurants = _restaurantService.GetAll(c => c.CreationDate.Date == NewDateValue, pageNumber, pageSize, out total);
            return PartialView("RestaurantsToday", new StaticPagedList<Restaurant>(restaurants, pageNumber, pageSize, total));
        }
        public ActionResult OrderDetails(int orderId)
        {
            var orders = _orderService.GetById(orderId);

            return PartialView("OrderDetail", orders);
        }
        public ActionResult RestaurantDetails(int restaurantId)
        {
            var restaurant = _restaurantService.GetById(restaurantId);

            return PartialView("RestaurantDetail", restaurant);
        }
    }
}