﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Web.Controllers;

namespace ToOrder.Web.Areas.Admin.Controllers
{
    public class RestaurantMenuItemController : BaseController
    {
        IMenuItemService _menuitemService;
        ICategoryService _categoryService;

        public RestaurantMenuItemController(IMenuItemService menuitemService, ICategoryService categoryService)
        {
            _menuitemService = menuitemService;
            _categoryService = categoryService;
        }

        // GET: Admin/RestaurantMenuItem
        public ActionResult Index()
        {
            return View();
        }

        // GET: Admin/RestaurantMenuCardItem/Create
        public ActionResult Create(int menucardId, int restaurantId)
        {
            MenuItem restaurantMenuCardItem = new MenuItem
            {
                MenuCardId = menucardId
            };

            ViewBag.CategoryId = _categoryService.GetAll(r => r.RestaurantId == restaurantId).Select(m => new SelectListItem { Value = m.Id.ToString(), Text = m.Name }).ToList();
            return PartialView("AddEditPartial", restaurantMenuCardItem);
        }

        //POST: Admin/RestaurantMenuCardItem/Create
        [HttpPost]
        public ActionResult Create(MenuItem restaurantMenuCardItem, int restaurantId)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _menuitemService.AddNew(restaurantMenuCardItem);
                    return RedirectToAction("Details", "RestaurantMenuCard", new { id = restaurantMenuCardItem.MenuCardId });
                }
                else
                {
                    this.Response.StatusCode = 400;
                    ViewBag.CategoryId = _categoryService.GetAll(r => r.RestaurantId == restaurantId).Select(m => new SelectListItem { Value = m.Id.ToString(), Text = m.Name }).ToList();
                    return PartialView("AddEditPartial", restaurantMenuCardItem);
                }
            }
            catch
            {
                ViewBag.CategoryId = _categoryService.GetAll(r => r.RestaurantId == restaurantId).Select(m => new SelectListItem { Value = m.Id.ToString(), Text = m.Name }).ToList();
                return PartialView("AddEditPartial", restaurantMenuCardItem);
            }
        }

        // GET: Admin/RestaurantMenuCardItem/Edit/5
        public ActionResult Edit(int menuitemId)
        {
            MenuItem restaurantMenuCardItem = _menuitemService.GetById(menuitemId);

            ViewBag.CategoryId = _categoryService.GetAll(r => r.RestaurantId == restaurantMenuCardItem.MenuCard.RestaurantId).Select(m => new SelectListItem { Value = m.Id.ToString(), Text = m.Name }).ToList();
            return PartialView("AddEditPartial", restaurantMenuCardItem);
        }

        // POST: Admin/RestaurantMenuCardItem/Edit/5
        [HttpPost]
        public ActionResult Edit(MenuItem restaurantMenuCardItem, int restaurantId)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _menuitemService.Update(restaurantMenuCardItem);
                    return RedirectToAction("Details", "RestaurantMenuCard", new { id = restaurantMenuCardItem.MenuCardId });
                }
                else
                {
                    this.Response.StatusCode = 400;
                    ViewBag.CategoryId = _categoryService.GetAll(r => r.RestaurantId == restaurantId).Select(m => new SelectListItem { Value = m.Id.ToString(), Text = m.Name }).ToList();
                    return PartialView("AddEditPartial", restaurantMenuCardItem);
                }
            }
            catch
            {
                ViewBag.CategoryId = _categoryService.GetAll(r => r.RestaurantId == restaurantId).Select(m => new SelectListItem { Value = m.Id.ToString(), Text = m.Name }).ToList();
                return PartialView("AddEditPartial", restaurantMenuCardItem);
            }
        }

        // GET: Admin/RestaurantMenuCardItem/Delete/5
        public ActionResult Delete(int menuitemId)
        {
            MenuItem restaurantMenuCardItem = _menuitemService.GetById(menuitemId);
            return PartialView("Delete", restaurantMenuCardItem);
        }

        // POST: Admin/RestaurantMenuCardItem/Delete/5
        [HttpPost]
        public ActionResult Delete(MenuItem restaurantMenuCardItem)
        {
            try
            {
                _menuitemService.Delete(restaurantMenuCardItem);
                return RedirectToAction("Details", "RestaurantMenuCard", new { id = restaurantMenuCardItem.MenuCardId });
            }
            catch
            {
                return View();
            }
        }
    }
}