﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Web.Controllers;

namespace ToOrder.Web.Areas.Admin.Controllers
{
    public class RestaurantCuisineController : BaseController
    {
        IRestaurantCuisineService _restaurantCuisineService;
        ICuisineService _cuisineService;

        public RestaurantCuisineController(
            IRestaurantCuisineService restaurantCuisineService,
            ICuisineService cuisineService
            )
        {
            _restaurantCuisineService = restaurantCuisineService;
            _cuisineService = cuisineService;
        }

        // GET: Admin/RestaurantCuisine
        public ActionResult Index(int restaurantId)
        {
            var restaurantCuisines = _restaurantCuisineService.GetAll(c => c.RestaurantId == restaurantId);
            ViewBag.RestaurantId = restaurantId;
            return PartialView(restaurantCuisines);
        }

        // GET: Admin/RestaurantCuisine/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/RestaurantCuisine/Create
        public ActionResult Create(int restaurantId)
        {
            RestaurantCuisine restaurantCuisine = new RestaurantCuisine
            {
                RestaurantId=restaurantId
            };

            ViewBag.CuisineId = _cuisineService.GetAll().ToList().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name}).ToList();
            return PartialView("AddEditPartial", restaurantCuisine);
        }

        // POST: Admin/RestaurantCuisine/Create
        [HttpPost]
        public ActionResult Create(RestaurantCuisine restaurantCuisine)
        {
            try
            {
                _restaurantCuisineService.AddNew(restaurantCuisine);

                return RedirectToAction("Index", new { restaurantId = restaurantCuisine.RestaurantId });
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                if (ex is InvalidOperationException)
                    ModelState.AddModelError("", "Please correct following errors:" + Environment.NewLine + ex.Message );
                else
                    ModelState.AddModelError("", ex.Message);

                ViewBag.CuisineId = _cuisineService.GetAll().ToList().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name }).ToList();
                return PartialView("AddEditPartial", restaurantCuisine);
            }
        }

        // GET: Admin/RestaurantCuisine/Edit/5
        public ActionResult Edit(int id)
        {
            RestaurantCuisine restaurantCuisine = _restaurantCuisineService.GetById(id);
            ViewBag.CuisineList = _cuisineService.GetAll().ToList().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name }).ToList();
            return PartialView("AddEditPartial", restaurantCuisine);
        }

        // POST: Admin/RestaurantCuisine/Edit/5
        [HttpPost]
        public ActionResult Edit(RestaurantCuisine restaurantCuisine)
        {
            try
            {
                _restaurantCuisineService.Update(restaurantCuisine);

                return RedirectToAction("Index", new { restaurantId = restaurantCuisine.RestaurantId });
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                if (ex is InvalidOperationException)
                    ModelState.AddModelError("", "Please correct following errors:");
                else
                    ModelState.AddModelError("", ex.Message);

                ViewBag.CuisineId = _cuisineService.GetAll().ToList().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name }).ToList();
                return PartialView("AddEditPartial", restaurantCuisine);
            }
        }

        // GET: Admin/RestaurantCuisine/Delete/5
        public ActionResult Delete(int id)
        {
            RestaurantCuisine restaurantCuisine = _restaurantCuisineService.GetById(id);
            return PartialView("Delete",restaurantCuisine);
        }

        // POST: Admin/RestaurantCuisine/Delete/5
        [HttpPost]
        public ActionResult Delete(RestaurantCuisine restaurantCuisine)
        {
            try
            {
                // TODO: Add delete logic here
                _restaurantCuisineService.Delete(restaurantCuisine);
                return RedirectToAction("Index", new { restaurantId = restaurantCuisine.RestaurantId });
            }
            catch
            {
                return View();
            }
        }
    }
}
