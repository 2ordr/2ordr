﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Web.Controllers;

namespace ToOrder.Web.Controllers
{
    public static class ControllerExtensions
    {
        public static int CurrentRestaurantId(this BaseController controller)
        {
            int restaurantId=0;
            var rsession = controller.Session["restaurantId"];
            
            if (rsession == null)
                return restaurantId;

            int.TryParse(rsession.ToString(), out restaurantId);

            return restaurantId;
        }

        public static string CurrentRestaurantName(this BaseController controller)
        {
            string restaurantName = "";
            var rsession = controller.Session["restaurantName"];

            if (rsession == null)
                return restaurantName;

            restaurantName = rsession.ToString();

            return restaurantName;
        }
    
    }
}