﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Web.Controllers;

namespace ToOrder.Web.Areas.RestaurantAdmin.Controllers
{


    public class MenuItemController : BaseController
    {

        IMenuItemService _menuItemService;
        IMenuCardService _menuCardService;
        ICategoryService _categoryService;

        public MenuItemController(IMenuItemService menuItemService, IMenuCardService menuCardService, ICategoryService categoryService)
        {
            _menuItemService = menuItemService;
            _menuCardService = menuCardService;
            _categoryService = categoryService;
        }

        // GET: RestaurantAdmin/MenuItem
        public ActionResult Index(int menuCardId)
        {

            var restaurantId = this.CurrentRestaurantId();
            if (restaurantId == 0)
                return RedirectToAction("MyRestaurants", "Restaurant");
            var items = _menuItemService.GetAll(i => i.MenuCardId == menuCardId);

            ViewBag.MenuCard = _menuCardService.GetById(menuCardId);

            return View(items);
        }

        // GET: RestaurantAdmin/MenuItem/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: RestaurantAdmin/MenuItem/Create
        public ActionResult Create(int menuCardId)
        {
            var restaurantId = this.CurrentRestaurantId();

            if (restaurantId == 0)
                return RedirectToAction("MyRestaurants", "Restaurant");

            var menuItem = new MenuItem { MenuCardId = menuCardId };
            ViewBag.CategoryId = _categoryService.GetAll(c => c.RestaurantId == restaurantId).ToList().Select(m => new SelectListItem { Value = m.Id.ToString(), Text = m.Name }).ToList();
            return View(menuItem);
        }

        // POST: RestaurantAdmin/MenuItem/Create
        [HttpPost]
        public ActionResult Create(MenuItem menuItem)
        {
            var restaurantId = this.CurrentRestaurantId();
            if (restaurantId == 0)
                return RedirectToAction("MyRestaurants", "Restaurant");

            try
            {
                // if restaurant id doesn't match
                if (_menuCardService.GetById(menuItem.MenuCardId).RestaurantId != restaurantId)
                    return RedirectToAction("MyRestaurants", "Restaurant");


                if (ModelState.IsValid)
                {
                    _menuItemService.AddNew(menuItem);
                    return RedirectToAction("Index", new { menuCardId = menuItem.MenuCardId });
                }

                return View(menuItem);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(menuItem);
            }
        }

        // GET: RestaurantAdmin/MenuItem/Edit/5
        public ActionResult Edit(int id)
        {
            var restaurantId = this.CurrentRestaurantId();

            if (restaurantId == 0)
                return RedirectToAction("MyRestaurants", "Restaurant");

            var menuItem = _menuItemService.GetById(id);
            ViewBag.CategoryId = _categoryService.GetAll(c => c.RestaurantId == restaurantId).ToList().Select(m => new SelectListItem { Value = m.Id.ToString(), Text = m.Name }).ToList();
            return View(menuItem);
        }

        // POST: RestaurantAdmin/MenuItem/Edit/5
        [HttpPost]
        public ActionResult Edit(MenuItem menuitem)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _menuItemService.Update(menuitem);
                    return RedirectToAction("Index", new { menuCardId = menuitem.MenuCardId });
                }

                return View(menuitem);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(menuitem);
            }
        }

        // GET: RestaurantAdmin/MenuItem/Delete/5
        public ActionResult Delete(int id)
        {
            MenuItem menuItem = _menuItemService.GetById(id);
            return PartialView("Delete", menuItem);
        }

        // POST: RestaurantAdmin/MenuItem/Delete/5
        [HttpPost]
        public ActionResult Delete(MenuItem menuitem)
        {
            try
            {
                _menuItemService.Delete(menuitem);
                return RedirectToAction("Index", new { menuCardId = menuitem.MenuCardId });
            }
            catch
            {
                return PartialView("Delete", menuitem);
            }
        }
    }
}
