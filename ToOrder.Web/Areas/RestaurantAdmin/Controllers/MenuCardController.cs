﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Web.Controllers;

namespace ToOrder.Web.Areas.RestaurantAdmin.Controllers
{
    [Authorize(Roles="Admin,RestaurantOwner,RestaurantUser")]
    public class MenuCardController : BaseController
    {
        IMenuCardService _menuCardService;
        IMenuItemService _menuItemService;
        public MenuCardController(IMenuCardService menuCardService, IMenuItemService menuItemService)
        {
            _menuCardService = menuCardService;
            _menuItemService = menuItemService;
        }

        // GET: RestaurantAdmin/MenuCard
        public ActionResult Index()
        {

            var restaurantId = this.CurrentRestaurantId();
            if (restaurantId == 0)
                return RedirectToAction("MyRestaurants", "Restaurant");
            var menuCards = _menuCardService.GetAll(c => c.RestaurantId == restaurantId);

            return View(menuCards);
        }

        // GET: RestaurantAdmin/MenuCard/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: RestaurantAdmin/MenuCard/Create
        public ActionResult Create()
        {
            MenuCard menuCard = new MenuCard();
            return View(menuCard);
        }

        // POST: RestaurantAdmin/MenuCard/Create
        [HttpPost]
        public ActionResult Create(MenuCard menuCard)
        {
            try
            {
                var restaurantId = this.CurrentRestaurantId();
                if (restaurantId == 0)
                    return RedirectToAction("MyRestaurants", "Restaurant");

                menuCard.RestaurantId = restaurantId;

                if (ModelState.IsValid)
                {
                    _menuCardService.AddNew(menuCard);
                    return RedirectToAction("Index");
                }

                return View(menuCard);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(menuCard);
            }
        }

        // GET: RestaurantAdmin/MenuCard/Edit/5
        public ActionResult Edit(int id)
        {
            var menuCard = _menuCardService.GetById(id);
            return View(menuCard);
        }

        // POST: RestaurantAdmin/MenuCard/Edit/5
        [HttpPost]
        public ActionResult Edit(MenuCard menuCard)
        {
            try
            {
                var restaurantId = this.CurrentRestaurantId();
                if (restaurantId == 0 )
                    return RedirectToAction("MyRestaurants", "Restaurant");

                menuCard.RestaurantId = restaurantId;

                if (ModelState.IsValid)
                {

                    _menuCardService.Update(menuCard);
                    return RedirectToAction("Index");
                }

                return View(menuCard);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(menuCard);
            }
        }

        // GET: RestaurantAdmin/MenuCard/Delete/5
        public ActionResult Delete(int id)
        {
            var menuCard = _menuCardService.GetById(id);
            var isMenuItemsLinked = _menuItemService.GetAll(m => m.MenuCardId == id).Count();
            if (isMenuItemsLinked > 0)
            {
                return PartialView("MenuItemsLinked", menuCard);
            }
            else
            {

                return PartialView(menuCard);
            }
        }

        // POST: RestaurantAdmin/MenuCard/Delete/5
        [HttpPost]
        public ActionResult Delete(MenuCard menuCard)
        {
            try
            {
                _menuCardService.Delete(menuCard);
                return RedirectToAction("Index");
            }
            catch
            {
             return  RedirectToAction("Index");
            }
        }
    }
}
