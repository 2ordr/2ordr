﻿using Newtonsoft.Json;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Web.App_Start;
using ToOrder.Web.Controllers;
using ToOrder.Localization;

namespace ToOrder.Web.Areas.RestaurantAdmin.Controllers
{
    [Authorize(Roles = "Admin,RestaurantOwner,RestaurantUser")]
    public class OrderController : BaseController
    {
        IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        // GET: RestaurantAdmin/Order
        public ActionResult Index(int? page)
        {

            var restaurantId = this.CurrentRestaurantId();
            if (restaurantId == 0)
                return RedirectToAction("MyRestaurants", "Restaurant");
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = ToOrderConfigs.DefaultPageSize;

            int total;
            ViewBag.Header = Resources.UnservedOrders;
            var orders = _orderService.GetAll(c => c.IsServed == false && c.RestaurantId == restaurantId, pageNumber, pageSize, out total);

            return View(new StaticPagedList<Order>(orders, pageNumber, pageSize, total));
        }
        public ActionResult Search(string order, int? page)
        {
            ViewBag.order = order;

            int pageNumber = page != null ? page.Value : 1;
            int pageSize = ToOrderConfigs.DefaultPageSize;
            var restaurantId = this.CurrentRestaurantId();
            int total;


            var orders = _orderService.GetAll(c => c.IsServed == false && c.RestaurantId == restaurantId, pageNumber, pageSize, out total);
            return View("Index", new StaticPagedList<Order>(orders, pageNumber, pageSize, total));
        }

        // GET: RestaurantAdmin/Order
        public ActionResult Served(int? page)
        {

            var restaurantId = this.CurrentRestaurantId();
            if (restaurantId == 0)
                return RedirectToAction("MyRestaurants", "Restaurant");
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = ToOrderConfigs.DefaultPageSize;
            int total;
            ViewBag.Header = Resources.PreviousOrders;
            var orders = _orderService.GetAll(c => c.IsServed && c.RestaurantId == restaurantId, pageNumber, pageSize, out total);

            return View("Index", new StaticPagedList<Order>(orders, pageNumber, pageSize, total));
        }

        // GET: RestaurantAdmin/Order/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: RestaurantAdmin/Order/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RestaurantAdmin/Order/Create
        [HttpPost]
        public ActionResult Create(Order order)
        {
            return View();
        }

        // GET: RestaurantAdmin/Order/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: RestaurantAdmin/Order/Edit/5
        [HttpPost]
        public ActionResult Edit(Order order)
        {
            return View();
        }

        // GET: RestaurantAdmin/Order/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: RestaurantAdmin/Order/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            return View();
        }

        public JsonResult GetBarGraphData()
        {

            var restaurantId = this.CurrentRestaurantId();
            var orders = _orderService.GetAll(c => c.RestaurantId == restaurantId && (c.Date > DateTime.Now.AddDays(-7))).OrderBy(x => x.Date).GroupBy(item => item.Date.Date).OrderBy(g => g.Key).
                        Select(i => new Order { Date = i.Key.Date, GrossAmount = i.Sum(w => w.GrossAmount) }).ToList();
            var from = DateTime.Now.AddDays(-7);

            var to = DateTime.Now.AddDays(-1);

            var days = Enumerable.Range(0, 1 + to.Subtract(from).Days)
                  .Select(offset => from.AddDays(offset))
                  .ToArray();

            var data = days.Select(i => new Order { Date = i.Date, GrossAmount = orders.Where(p => p.Date == i.Date).Sum(w => w.GrossAmount) }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetLineGraphData()
        {


            var restaurantId = this.CurrentRestaurantId();
            var orders = _orderService.GetAll(c => c.RestaurantId == restaurantId && (c.Date > DateTime.Now.AddDays(-30))).OrderBy(x => x.Date).GroupBy(item => item.Date.Date).OrderBy(g => g.Key).
                        Select(i => new Order { Date = i.Key.Date, GrossAmount = i.Sum(w => w.GrossAmount) }).ToList();
            var from = DateTime.Now.AddDays(-30);

            var to = DateTime.Now.AddDays(-1);

            var days = Enumerable.Range(0, 1 + to.Subtract(from).Days)
                  .Select(offset => from.AddDays(offset))
                  .ToArray();

            var data = days.Select(i => new Order { Date = i.Date, GrossAmount = orders.Where(p => p.Date == i.Date).Sum(w => w.GrossAmount) }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult BarGraphOrder()
        {
            return View();
        }
        public ActionResult LineGraphOrder()
        {
            return View();
        }
        public ActionResult UnservedOrders(int? page)
        {
            var restaurantId = this.CurrentRestaurantId();
            if (restaurantId == 0)
                return RedirectToAction("MyRestaurants", "Restaurant");
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = 6;
            int total;
            var orders = _orderService.GetAll(c => c.IsServed == false && c.RestaurantId == restaurantId, pageNumber, pageSize, out total);
            //return View(orders);
            return View(new StaticPagedList<Order>(orders, pageNumber, pageSize, total));
        }
        public ActionResult SearchUnservedOrders(string order, int? page)
        {
            ViewBag.orders = order;
            var restaurantId = this.CurrentRestaurantId();
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = 6;

            int total;


            var orders = _orderService.GetAll(c => c.IsServed == false && c.RestaurantId == restaurantId, pageNumber, pageSize, out total);
            return PartialView("UnservedOrders", new StaticPagedList<Order>(orders, pageNumber, pageSize, total));
        }
        public ActionResult LatestOrders(int? page)
        {
            DateTime NewDateValue = System.DateTime.Now.AddDays(-2);
            var restaurantId = this.CurrentRestaurantId();
            if (restaurantId == 0)
                return RedirectToAction("MyRestaurants", "Restaurant");
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = 6;
            int total;
            var orders = _orderService.GetAll(c => c.RestaurantId == restaurantId && c.Date > NewDateValue, pageNumber, pageSize, out total).OrderByDescending(x => x.Date);
            // return View(orders);
            return View(new StaticPagedList<Order>(orders, pageNumber, pageSize, total));
        }
        public ActionResult SearchLatestOrders(string order, int? page)
        {
            DateTime NewDateValue = System.DateTime.Now.AddDays(-2);
            ViewBag.orders = order;
            var restaurantId = this.CurrentRestaurantId();
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = 6;

            int total;


            var orders = _orderService.GetAll(c => c.RestaurantId == restaurantId && c.Date > NewDateValue, pageNumber, pageSize, out total).OrderByDescending(x => x.Date);
            return PartialView("UnservedOrders", new StaticPagedList<Order>(orders, pageNumber, pageSize, total));
        }

        public ActionResult ShowBarGraphOrders(int? page)
        {
            var restaurantId = this.CurrentRestaurantId();
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = ToOrderConfigs.DefaultPageSize;

            int total;
            var orders = _orderService.GetAll(c => c.RestaurantId == restaurantId && (c.Date > DateTime.Now.AddDays(-7)), pageNumber, pageSize, out total);
            ViewBag.Header = "Last Seven Days Orders";
            return View("Index", new StaticPagedList<Order>(orders, pageNumber, pageSize, total));
        }
        public ActionResult ShowLineGraphOrders(int? page)
        {
            var restaurantId = this.CurrentRestaurantId();
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = ToOrderConfigs.DefaultPageSize;

            int total;
            var orders = _orderService.GetAll(c => c.RestaurantId == restaurantId && (c.Date > DateTime.Now.AddDays(-30)), pageNumber, pageSize, out total);
            ViewBag.Header = "Last 30 Days  Orders";
            return View("Index", new StaticPagedList<Order>(orders, pageNumber, pageSize, total));
        }
        public ActionResult UnservedOrderDetails(int Id, int? page)
        {
            var restaurantId = this.CurrentRestaurantId();
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = ToOrderConfigs.DefaultPageSize;

            int total;
            var orders = _orderService.GetAll(c => c.RestaurantId == restaurantId && (c.Id == Id), pageNumber, pageSize, out total);
            ViewBag.Header = "Unserved Order";
            return View("Index", new StaticPagedList<Order>(orders, pageNumber, pageSize, total));
        }
        public ActionResult LatestOrderDetails(int Id, int? page)
        {
            var restaurantId = this.CurrentRestaurantId();
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = ToOrderConfigs.DefaultPageSize;

            int total;
            var orders = _orderService.GetAll(c => c.RestaurantId == restaurantId && (c.Id == Id), pageNumber, pageSize, out total);
            ViewBag.Header = "Latest Order";
            return View("Index", new StaticPagedList<Order>(orders, pageNumber, pageSize, total));
        }
    }
}