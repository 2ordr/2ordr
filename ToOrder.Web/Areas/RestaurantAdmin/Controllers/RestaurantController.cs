﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Web.Controllers;


namespace ToOrder.Web.Areas.RestaurantAdmin.Controllers
{
    [Authorize(Roles = "Admin,RestaurantOwner, RestaurantUser")]
    public class RestaurantController : BaseController
    {
        IRestaurantService _restaurantService;
        IUserService _userServce;

        public RestaurantController(IRestaurantService restaurantService, IUserService userService)
        {
            _restaurantService = restaurantService;
            _userServce = userService;
        }


        public ActionResult SelectRestaurant(int id)
        {
            Session["restaurantId"] = id;
            var restaurantName = _restaurantService.GetById(id).Name;
            Session["restaurantName"] = restaurantName;
            return RedirectToAction("Index");
        }


        // GET: RestaurantAdmin/Restaurant
        public ActionResult Index()
        {
            int id = this.CurrentRestaurantId(); ;
            var restaurant = _restaurantService.GetById(id);

            return RedirectToAction("Dashboard");
        }

        [Authorize(Roles = "RestaurantOwner")]

        public ActionResult MyRestaurants()
        {
                var user = GetAuthenticatedUser();

                if (user == null)
                    return Content("Not authorized!");

                var restaurants = _restaurantService.GetAll(r => r.RestaurantUsers.Where(u => u.UserId == user.Id).Any());

                if (restaurants.Count() > 1)
                    return View(restaurants);
                else if (restaurants.Count() == 1)
                {
                    Session["restaurantId"] = restaurants.First().Id;
                    return Content("Show Restaurant Individual Page");
                }
                else
                {
                    return Content("No restaurants under your email id");
                }

            }


        public ActionResult Edit()
        {
            var restaurantId = this.CurrentRestaurantId();

            if (restaurantId == 0)
                return RedirectToAction("MyRestaurants");

            var restaurant = _restaurantService.GetById(restaurantId);

            return View(restaurant);
        }


        [HttpPost]
        public ActionResult Edit(Restaurant restaurant, HttpPostedFileBase uploadFile)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string folderPath = @"\Pictures\Restaurant";
                    string folderPathOnServer = Server.MapPath(folderPath);
                    if (!Directory.Exists(folderPathOnServer))
                    {
                        Directory.CreateDirectory(folderPathOnServer);
                    }
                    if (uploadFile != null)
                    {
                        if (uploadFile.ContentLength <= 2000000)
                        {
                            var allowedExtensions = new[] { ".jpg", ".png", ".gif", ".jpeg" };
                            var filename = Path.GetFileName(uploadFile.FileName);
                            var ext = Path.GetExtension(uploadFile.FileName);

                            if (allowedExtensions.Contains(ext))
                            {
                                restaurant.PictureFileName = filename;
                                _restaurantService.Update(restaurant);

                                int resID = restaurant.Id;
                                string myPicture = resID + "_" + filename;
                                var path = Path.Combine(folderPathOnServer, myPicture);
                                uploadFile.SaveAs(path);

                                return RedirectToAction("Edit");
                            }
                            else
                            {
                                ModelState.AddModelError("", "Image Extension is not Valid");
                                return View(restaurant);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "Please Select Proper Image ");
                            return View(restaurant);
                        }
                    }
                    else
                    {
                        _restaurantService.Update(restaurant);
                        return RedirectToAction("Edit");
                    }
                }
                else
                {
                    return RedirectToAction("Edit");
                }
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                if (ex is InvalidOperationException)
                    ModelState.AddModelError("", "Please correct following errors:");
                else
                    ModelState.AddModelError("", ex.Message);
                return RedirectToAction("Edit");
            }
        }

        public ActionResult Dashboard()
        {
            var restaurantId = this.CurrentRestaurantId();
            if (restaurantId == 0)
                return RedirectToAction("MyRestaurants", "Restaurant");
            return View();
        }
    }
}