﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Web.Controllers;

namespace ToOrder.Web.Areas.RestaurantAdmin.Controllers
{
    public class UserController : BaseController
    {
        IUserRestaurantService _userRestaurantService;
        IUserService _userService;
        IRoleService _roleService;
        IUserRoleService _userRoleService;
        public UserController(IUserRestaurantService userRestaurantService, IUserService userService, IRoleService roleService, IUserRoleService userRoleService)
        {
            _userRestaurantService = userRestaurantService;
            _userService = userService;
            _roleService = roleService;
            _userRoleService = userRoleService;
        }

        // GET: RestaurantAdmin/User
        public ActionResult Index()
        {
            var restaurantId = this.CurrentRestaurantId();
            if (restaurantId == 0)
                return RedirectToAction("MyRestaurants", "Restaurant");
            var users = _userRestaurantService.GetAll(u => u.RestaurantId == restaurantId);
            return View(users);
        }

        public ActionResult Create()
        {
            UserRestaurant users = new UserRestaurant();
            var restaurantId = this.CurrentRestaurantId();
            return View(users);
        }
        [HttpPost]
        public ActionResult Create(UserRestaurant users)
        {
            try
            {
                var restaurantId = this.CurrentRestaurantId();
                if (restaurantId == 0)
                    return RedirectToAction("MyRestaurants", "Restaurant");

                users.RestaurantId = restaurantId;
                var user = _userService.GetAll(u => u.Email == users.User.Email).FirstOrDefault();
                if (user == null)
                {
                    ModelState.AddModelError("", "Email Id does not exist");
                    return View(users);
                }
                else
                {
                    users.User = user;
                    _userRestaurantService.AddNew(users);
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(users);
            }
        }
        public ActionResult Delete(int id)
        {
            var userRestaurant = _userRestaurantService.GetById(id);
            return PartialView(userRestaurant);
        }

        [HttpPost]
        public ActionResult Delete(UserRestaurant userRestaurant)
        {
            try
            {
                _userRestaurantService.Delete(userRestaurant);
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }
    }
}