﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Web.Controllers;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Web.App_Start;
using ToOrder.Localization;

namespace ToOrder.Web.Areas.RestaurantAdmin.Controllers
{
    [Authorize(Roles = "Admin,RestaurantOwner,RestaurantUser")]
    public class OrderDetailController : BaseController
    {
        IOrderDetailService _orderDetailService;

        public OrderDetailController(IOrderDetailService orderDetailService)
        {
            _orderDetailService = orderDetailService;
        }

        // GET: RestaurantAdmin/OrderDetail
        public ActionResult Index()
        {
            return View();
        }

        // GET: RestaurantAdmin/OrderDetail/Details/5
        public ActionResult Details(int orderId)
        {
            var orderDetails = _orderDetailService.GetAll(o => o.OrderId == orderId);

            return PartialView("OrderDetailsPartial", orderDetails.ToList());
        }

        // GET: RestaurantAdmin/OrderDetail/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RestaurantAdmin/OrderDetail/Create
        [HttpPost]
        public ActionResult Create(OrderDetails orderDetails)
        {
            return View();
        }

        // GET: RestaurantAdmin/OrderDetail/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: RestaurantAdmin/OrderDetail/Edit/5
        [HttpPost]
        public ActionResult Edit(OrderDetails orderDetails)
        {
            return View();
        }

        // GET: RestaurantAdmin/OrderDetail/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: RestaurantAdmin/OrderDetail/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            return View();
        }
    }
}