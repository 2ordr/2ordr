﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;
using ToOrder.Web.App_Start;
using ToOrder.Web.Controllers;

namespace ToOrder.Web.Areas.RestaurantAdmin.Controllers
{
    [Authorize(Roles="admin,RestaurantOwner,RestaurantUser")]
    public class CategoryController : BaseController
    {
        ICategoryService _categoryService;
        IMenuItemService _menuItemService;
        public CategoryController(ICategoryService categoryService, IMenuItemService menuItemService)
        {
            _categoryService = categoryService;
            _menuItemService = menuItemService;
        }

        // GET: RestaurantAdmin/Category
        public ActionResult Index(int? page)
        {
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = ToOrderConfigs.DefaultPageSize;

            int total;

            var restaurantId = this.CurrentRestaurantId();
            if (restaurantId == 0)
                return RedirectToAction("MyRestaurants", "Restaurant");

            var categories = _categoryService.GetAll(c => c.RestaurantId == restaurantId, pageNumber, pageSize, out total);

            return View(new StaticPagedList<Category>(categories, pageNumber, pageSize, total));
        }

        public ActionResult Search(int? page)
        {
            int pageNumber = page != null ? page.Value : 1;
            int pageSize = ToOrderConfigs.DefaultPageSize;

            int total;

            var restaurantId = this.CurrentRestaurantId();
            if (restaurantId == 0)
                return RedirectToAction("MyRestaurants", "Restaurant");

            var categories = _categoryService.GetAll(c => c.RestaurantId == restaurantId, pageNumber, pageSize, out total);
            return View("Index", new StaticPagedList<Category>(categories, pageNumber, pageSize, total));
        }

        // GET: RestaurantAdmin/Category/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: RestaurantAdmin/Category/Create
        public ActionResult Create()
        {
            var category = new Category();
            return View(category);
            
        }

        // POST: RestaurantAdmin/Category/Create
        [HttpPost]
        public ActionResult Create(Category category)
        {
            try
            {
                var restaurantId = this.CurrentRestaurantId();
                if (restaurantId == 0)
                    return RedirectToAction("MyRestaurants", "Restaurant");

                category.RestaurantId = restaurantId;

                if(ModelState.IsValid)
                {

                    _categoryService.AddNew(category);
                    return RedirectToAction("Index");

                }

                return View(category);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(category);
            }
        }

        // GET: RestaurantAdmin/Category/Edit/5
        public ActionResult Edit(int id)
        {
            var category = _categoryService.GetById(id);
            return View(category);
        }

        // POST: RestaurantAdmin/Category/Edit/5
        [HttpPost]
        public ActionResult Edit(Category category)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _categoryService.Update(category);
                    return RedirectToAction("Index");
                }

                return View(category);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(category);
            }
        }

        // GET: RestaurantAdmin/Category/Delete/5
        public ActionResult Delete(int id)
        {

            var category = _categoryService.GetById(id);
            var isMenuItemsLinked = _menuItemService.GetAll(c => c.CategoryId == id).Count();
            if (isMenuItemsLinked > 0)
            {
                return PartialView("ItemsLinked", category);
            }
            else
            {
                return PartialView("Delete",category);
            }

        }

        // POST: RestaurantAdmin/Category/Delete/5
        [HttpPost]
        public ActionResult Delete(Category category)
        {
            try
            {
                _categoryService.Delete(category);
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }
    }
}
