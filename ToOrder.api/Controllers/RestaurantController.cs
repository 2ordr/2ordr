﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ToOrder.api.Models;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;

namespace ToOrder.api.Controllers
{

    public class SearchBinding
    {
        public string Keywords { get; set; }
        public string CuisineIds { get; set; }

        public string Budget { get; set; }

        public string Place { get; set; }
    }


    [RoutePrefix("api/Restaurant")]
    public class RestaurantController : ApiController
    {
        IRestaurantService _restaurantService;
        public RestaurantController(IRestaurantService restaurantService)
        {
            _restaurantService = restaurantService;
        }

        // GET: api/Restaurant
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }


        [HttpGet]
        [Route("search")]
        public IEnumerable<object> Search(SearchRestaurants search)
        {
            List<int> cuisineList = null;

            if (!string.IsNullOrWhiteSpace(search.Cuisines))
                cuisineList = search.Cuisines.Split(',').Select(int.Parse).ToList();

            var restaurants = _restaurantService.Search(search.Keywords, search.Latitude, search.Longitude, search.Radius, cuisineList, search.Sort, search.Order).Select(x => new { id = x.Id, Name = x.Name });
            return restaurants;
        }


        [HttpPost]
        [Route("search")]
        public IEnumerable<object> SearchRestaurants(SearchBinding search)
        {
            List<int> cuisineList = null;
            List<int> budgetList = null;

            if (!string.IsNullOrWhiteSpace(search.CuisineIds))
                cuisineList = search.CuisineIds.Split(',').Select(int.Parse).ToList();

            if (!string.IsNullOrWhiteSpace(search.Budget))
                budgetList = search.Budget.Split(',').Select(int.Parse).ToList();

            var restaurants = _restaurantService.SearchRestaurants(search.Keywords, cuisineList, budgetList, search.Place).Select(x => new { id = x.Id, Name = x.Name });
            return restaurants;
        }


        // GET: api/Restaurant/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Restaurant
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Restaurant/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Restaurant/5
        public void Delete(int id)
        {
        }




    }
}
