[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(ToOrder.api.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(ToOrder.api.App_Start.NinjectWebCommon), "Stop")]

namespace ToOrder.api.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using ToOrder.Data;
    using ToOrder.Core.Data;
    using ToOrder.Core.Services.Interfaces;
    using ToOrder.Core.Services.Implementations;
    using System.Web.Http;
    using Ninject.WebApi.DependencyResolver;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {

            kernel.Bind<IDbContext>().To<ToOrderDbContext>().InSingletonScope();
            kernel.Bind(typeof(IRepository<>)).To(typeof(EfRepository<>));



            kernel.Bind<IUserService>().To<UserService>();
            kernel.Bind<IRoleService>().To<RoleService>();
            kernel.Bind<IRestaurantService>().To<RestaurantService>();
            kernel.Bind<ICuisineService>().To<CuisineService>();
            kernel.Bind<IRestaurantCuisineService>().To<RestaurantCuisineService>();
            kernel.Bind<IDinningTableService>().To<DinningTableService>();
            kernel.Bind<IMenuCardService>().To<MenuCardService>();
            kernel.Bind<IRatingService>().To<RatingService>();
            kernel.Bind<IMenuItemService>().To<MenuItemService>();
            kernel.Bind<ICategoryService>().To<CategoryService>();
            kernel.Bind<IOrderService>().To<OrderService>();
            kernel.Bind<IUserCardService>().To<UserCardService>();
            kernel.Bind<IBannerService>().To<BannerService>();
            kernel.Bind<IUserRestaurantService>().To<UserRestaurantService>();
            kernel.Bind<IUserRoleService>().To<UserRoleService>();
            kernel.Bind<IUserFavoriteService>().To<UserFavoriteService>();
            kernel.Bind<IOrderDetailService>().To<OrderDetailService>();
        }        
    }
}
