﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToOrder.api.Models
{
    public class SearchRestaurants
    {
        public string Keywords { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public double Radius { get; set; }

        public string Cuisines { get; set; }

        public string Sort { get; set; }

        public string Order { get; set; }

        public int Count { get; set; }

        public int Start { get; set; }


    }
}