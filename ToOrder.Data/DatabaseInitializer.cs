﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Entities;

namespace ToOrder.Data
{
    public class DatabaseInitializer : DropCreateDatabaseIfModelChanges<ToOrderDbContext>  //DropCreateDatabaseIfModelChanges
    {
        protected override void Seed(ToOrderDbContext context)
        {
            try
            {
                Random rnd = new Random();
                string[] personNames = { "support", "rest", "Andrew", "Peter", "Maria", "Sonia", "John", "Steven", "Alex", "Sanjay", "Mike", "Diago" };
                string[] restaurantNames = { "Estuary Cafe", "9 Bar ", "WOO BAR - W Goa ", "Cafe Lilliput", "Nyex Beach", "Blue Bird", "Salt & Pepper", "The Seaside Pizza Bar", "Zaffran", "Baba Wood Cafe", "Viva Panjim", "Mosaic Restaurant", "Mainland China", "Domino's Pizza", "AZ.U.R - The Marriott", "Cafe Bhosle", "Tatos", "Hotel Sanman", "Hotel Sainath" };
                string[] locality = { "Panaji", "Ponda", "Margao", "Mapusa", "Anjuna", "Miramar" };

                for (int i = 0; i < personNames.Length - 1; i++)
                {
                    User user = new User
                    {
                        FirstName = personNames[i],
                        LastName = "Nobel",

                        Telephone = rnd.Next(12121212, 98989898).ToString(),
                        FaxNumber = rnd.Next(12121212, 98989898).ToString(),
                        Mobile = rnd.Next(12121212, 98989898).ToString(),
                        Email = personNames[i] + "@certigoa.com",
                        Password = "pass1234"
                    };

                    Address address = new Address
                    {
                        Street = "Curti",
                        Locality = "Ponda",
                        Region = "Goa",
                        Country = "India",
                    };

                    user.Address = address;

                    context.Set<User>().Add(user);
                }
                context.SaveChanges();

                Role role1 = new Role { Name = "Admin" };
                Role role2 = new Role { Name = "RestaurantOwner" };
                Role role3 = new Role { Name = "RestaurantUser" };
                Role role4 = new Role { Name = "Customer" };

                context.Set<Role>().Add(role1);
                context.Set<Role>().Add(role2);
                context.Set<Role>().Add(role3);
                context.Set<Role>().Add(role4);
                context.SaveChanges();


                //first user admin
                UserRole userRole1 = new UserRole
                {
                    UserId = 1,
                    RoleId = 1
                };

                context.Set<UserRole>().Add(userRole1);


                //second user blue restaurnat
                UserRole userRole2 = new UserRole
                {
                    UserId = 2,
                    RoleId = 2
                };
                context.Set<UserRole>().Add(userRole2);


                for (int i = 3; i < personNames.Length; i++)
                {
                    UserRole userRole = new UserRole
                    {
                        UserId = i, //ids in database starts from 1, not 0
                        RoleId = rnd.Next(1, 4)
                    };

                    context.Set<UserRole>().Add(userRole);
                }
                context.SaveChanges();



                //Restaurants

                for (int i = 0; i < restaurantNames.Length; i++)
                {
                    Restaurant restaurant = new Restaurant
                    {
                        Name = restaurantNames[i],
                        Address = new Address { Street = "MG Road", Region = "Goa", Locality = locality[rnd.Next(locality.Length)], Country = "India" },
                        Description = "Best Indian",
                        Contact = new ContactInfo { Email = "ka@asdf.com", FaxNumber = "234324", Telephone = "123123", Website = @"http://222.asdf.com" },
                        Budget = rnd.Next(1, 4),
                        Latitude = Convert.ToDouble(0.00M),
                        Longitude = Convert.ToDouble(0.00M),
                        IsSponsored = true,
                        Zoom = 13,
                        CreationDate = System.DateTime.Now
                    };
                    context.Set<Restaurant>().Add(restaurant);
                }
                context.SaveChanges();


                //User Restaurants

                var userRestaurant = new UserRestaurant { RestaurantId = 1, UserId = 2 };
                var userRestaurant2 = new UserRestaurant { RestaurantId = 2, UserId = 2 };
                context.Set<UserRestaurant>().Add(userRestaurant);
                context.Set<UserRestaurant>().Add(userRestaurant2);


                //cuisines

                string[] cuisines = { "Greek", "Chinese", "Thai", "Louisiana", "Maharashtrian", "Malayali", "Parsi", "Punjabi", "Rajasthani", "South Indian", "Pizza", "Wines" };

                //Cuisines
                for (int i = 0; i < cuisines.Length; i++)
                {
                    Cuisine cuisine = new Cuisine
                    {
                        Name = cuisines[i],
                        CreationDate = System.DateTime.Now.AddHours(rnd.Next(100))
                    };
                    context.Set<Cuisine>().Add(cuisine);
                }
                context.SaveChanges();


                for (int i = 1; i <= restaurantNames.Length; i++)
                {
                    List<int> randomList = new List<int>();

                    for (int j = 1; j < 6; j++)
                    {
                        int no = rnd.Next(1, cuisines.Length - 1);

                        if (!randomList.Contains(no))
                            randomList.Add(no);
                    }

                    foreach (var cuisineId in randomList)
                    {
                        RestaurantCuisine restaurantCuisine = new RestaurantCuisine { RestaurantId = i, CuisineId = cuisineId };
                        context.Set<RestaurantCuisine>().Add(restaurantCuisine);
                    }
                    context.SaveChanges();
                }


                //Menucards
                string[] menuCards = { "Default Menu", "New Year Menu" };
                //Category
                string[] categories = { "Soups", "Sandwiches", "Starters", "Main Course" };


                for (int i = 1; i < restaurantNames.Length; i++)
                {
                    MenuCard menuCardDefault = new MenuCard
                    {
                        Name = menuCards[0],

                        Description = "Default menu of the restaurant",

                        RestaurantId = i,

                        IsDefault = true,
                    };
                    context.Set<MenuCard>().Add(menuCardDefault);

                    MenuCard menuCard = new MenuCard
                    {
                        Name = menuCards[1],

                        Description = "New year special menu",

                        RestaurantId = i,

                        IsDefault = false,
                    };
                    context.Set<MenuCard>().Add(menuCard);


                    context.SaveChanges();


                    //Category


                    int catCount = rnd.Next(1, categories.Length);

                    for (int j = 1; j <= catCount; j++)
                    {
                        string cat = categories[j];

                        Category category = new Category
                        {
                            Name = cat,
                            Description = cat + " description here",
                            RestaurantId = i

                        };
                        context.Set<Category>().Add(category);
                        context.SaveChanges();

                        int itmCount = rnd.Next(2, 20);

                        for (int l = 1; l < itmCount; l++)
                        {
                            MenuItem menuItem = new MenuItem
                            {
                                Name = cat + " item" + l.ToString(),
                                Description = cat + " item Description",
                                MenuCard = menuCardDefault,
                                Price = rnd.Next(1, 1000),
                                IsActive = Convert.ToBoolean(1),
                                Category = category
                            };
                            context.Set<MenuItem>().Add(menuItem);

                        }
                    }
                }
                context.SaveChanges();


                //DinningTable

                for (int i = 1; i < 10; i++)
                {
                    DinningTable dinningrTable = new DinningTable
                    {
                        Name = "Table" + i.ToString(),
                        SeatCount = i + 2,

                        MenuCardId = i,
                    };
                    context.Set<DinningTable>().Add(dinningrTable);
                }

                context.SaveChanges();

                //Rating

                for (int i = 1; i < 10; i++)
                {
                    Rating rating = new Rating
                    {
                        Title = "Rating" + i.ToString(),
                        Description = "Best Hostel",
                        Ratings = rnd.Next(1, 4),
                        Date = System.DateTime.Now.AddHours(rnd.Next(100)),
                        RestaurantId = i,
                        UserId = i,
                        Blocked = Convert.ToBoolean(rnd.Next(0, 1)),

                    };
                    context.Set<Rating>().Add(rating);
                }

                context.SaveChanges();




                //Order
                for (int i = 1; i < 10; i++)
                {
                    Order order = new Order
                    {
                        UserId = i,
                        RestaurantId = i,
                        Date = System.DateTime.Now.AddHours(rnd.Next(100)),
                        GrossAmount = 200,
                        Tax = 25,
                        NetAmount = 250,
                        PaymentId = i,
                        IsServed = Convert.ToBoolean(rnd.Next(0, 1)),
                    };
                    context.Set<Order>().Add(order);
                }
                context.SaveChanges();

                //OrderDetails
                for (int i = 1; i < 10; i++)
                {
                    OrderDetails orderDetails = new OrderDetails
                    {
                        OrderId = i,
                        MenuItemId = i,
                        Qty = i
                    };
                    context.Set<OrderDetails>().Add(orderDetails);
                }
                context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }

                throw;
            }
        }
    }
}
