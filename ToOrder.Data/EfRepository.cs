﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Data;
using ToOrder.Core.Entities;

namespace ToOrder.Data
{
    public class EfRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly IDbContext _context;
        private IDbSet<TEntity> _entities;

        public EfRepository(IDbContext context)
        {
            _context = context;
        }

        protected virtual IDbSet<TEntity> Entities
        {
            get
            {
                if (_entities == null)
                    _entities = _context.Set<TEntity>();
                return _entities;
            }
        }




        public TEntity GetById(object id)
        {
            return this.Entities.Find(id);
        }

        public void Insert(TEntity entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                this.Entities.Add(entity);
                _context.SaveChanges();
                return ;
            }
            catch (DbEntityValidationException dbEx)
            {

                throw new Exception(GetFullErrorText(dbEx), dbEx);
            }
        }

        public void Insert(IEnumerable<TEntity> entities)
        {
            try
            {
                if (entities == null)
                    throw new ArgumentNullException("entities");

                foreach (var entity in entities)
                {
                    Entities.Add(entity);
                }
                _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {

                throw new Exception(GetFullErrorText(dbEx), dbEx);
            }
        }

        public void Update(TEntity entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                var retrievedEntity = GetById(entity.Id);

                if (retrievedEntity == null)
                    return;

                ((ToOrderDbContext)_context).Entry(retrievedEntity).CurrentValues.SetValues(entity);

                this._context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                throw new Exception(GetFullErrorText(dbEx), dbEx);
            }
        }


        public void Update(TEntity entity,string[] excludePropertyNames)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                var retrievedEntity = GetById(entity.Id);

                if (retrievedEntity == null)
                    return;

                var properties = typeof(TEntity).GetProperties().Where(x => !excludePropertyNames.Contains(x.Name)).ToList();

                foreach (var property in properties)
                {
                    var propertyValue = property.GetValue(entity);
                    retrievedEntity.GetType().GetProperty(property.Name).SetValue(retrievedEntity, propertyValue);
                }


                this._context.SaveChanges();

            }
            catch (DbEntityValidationException dbEx)
            {
                throw new Exception(GetFullErrorText(dbEx), dbEx);
            }
        }


        public void Update(IEnumerable<TEntity> entities)
        {
            try
            {
                if (entities == null)
                    throw new ArgumentNullException("entities");

                this._context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                throw new Exception(GetFullErrorText(dbEx), dbEx);
            }
        }

        public void Delete(TEntity entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                var oldEntity = GetById(entity.Id);
                this.Entities.Remove(oldEntity);

                this._context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                throw new Exception(GetFullErrorText(dbEx), dbEx);
            }
        }

        public void Delete(IEnumerable<TEntity> entities)
        {
            try
            {
                if (entities == null)
                    throw new ArgumentNullException("entities");

                foreach (var entity in entities)
                    this.Entities.Remove(entity);

                this._context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                throw new Exception(GetFullErrorText(dbEx), dbEx);
            }
        }

        public IQueryable<TEntity> Table
        {
            get
            {
                return this.Entities;
            }
        }

        public IList<TEntity> StoredProcedureList<TEntity>(string command, params object[] parameters)
            where TEntity : BaseEntity, new()
        {
            return _context.ExecuteStoredProcedureList<TEntity>(command, parameters);
        }

        protected string GetFullErrorText(DbEntityValidationException exc)
        {
            var msg = string.Empty;
            foreach (var validationErrors in exc.EntityValidationErrors)
                foreach (var error in validationErrors.ValidationErrors)
                    msg += string.Format("Property: {0} Error: {1}", error.PropertyName, error.ErrorMessage) + Environment.NewLine;
            return msg;
        }

    }
}
