﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Data;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;

namespace ToOrder.Core.Services.Implementations
{
    public class UserRestaurantService : ImplementationBase<IRepository<UserRestaurant>, UserRestaurant>, IUserRestaurantService
    {
        IRepository<UserRestaurant> _userRestaurantRepository;
        IRepository<User> _userRepository;
        IRepository<Role> _roleRepository;
        IRepository<UserRole> _userRoleRepository;

        public UserRestaurantService(IRepository<UserRestaurant> userRestaurantRepository, IRepository<User> userRepository, IRepository<Role> roleRepository, IRepository<UserRole> userRoleRepository)
            : base(userRestaurantRepository)
        {
            _userRestaurantRepository = userRestaurantRepository;
            _userRepository = userRepository;
            _roleRepository = roleRepository;
            _userRoleRepository = userRoleRepository;
        }
        public override void AddNew(UserRestaurant entity)
        {
            var user = _userRepository.Table.FirstOrDefault(u => u.Email == entity.User.Email);
            if (user == null)
            {
                throw new InvalidOperationException("Email Id does not exist");
            }
            var count = base.GetAll(u => u.User.Email == entity.User.Email && u.RestaurantId == entity.RestaurantId).Count();

            if (count > 0)
                throw new InvalidOperationException("User with this email id already exists");
            base.AddNew(entity);

            var roleId = _roleRepository.Table.FirstOrDefault(r => r.Name == "RestaurantUser").Id;
            UserRole userrole = new UserRole();
            userrole.RoleId = roleId;
            userrole.User = user;
            var userCount = _userRoleRepository.Table.FirstOrDefault(k=>k.RoleId==roleId && k.UserId==user.Id);
            if (userCount==null)
            _userRoleRepository.Insert(userrole);

        }
        public void Delete(UserRestaurant entity)
        {
            base.Delete(entity);
            var count = base.GetAll(u => u.User.Email == entity.User.Email).Count();
            if (count == 0)
            {
                var roleId = _roleRepository.Table.FirstOrDefault(r => r.Name == "RestaurantUser").Id;
                var userRole = _userRoleRepository.Table.FirstOrDefault(u => u.UserId == entity.User.Id && u.RoleId == roleId);
                _userRoleRepository.Delete(userRole);
            }
        }
    }
}
