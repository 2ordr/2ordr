﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Data;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;

namespace ToOrder.Core.Services.Implementations
{
    public class RatingService : ImplementationBase<IRepository<Rating>, Rating>, IRatingService
    {
        IRepository<Rating> _ratingRepository;

        public RatingService(IRepository<Rating> ratingRepository)
            : base(ratingRepository)
        {
            _ratingRepository = ratingRepository;
        }
    }
}
