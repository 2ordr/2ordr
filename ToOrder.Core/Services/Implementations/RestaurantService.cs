﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Data;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;

namespace ToOrder.Core.Services.Implementations
{
    public class RestaurantService : ImplementationBase<IRepository<Restaurant>, Restaurant>, IRestaurantService
    {
        IRepository<Restaurant> _restaurantRepository;

        public RestaurantService(IRepository<Restaurant> restaurantRepository)
            : base(restaurantRepository)
        {
            _restaurantRepository = restaurantRepository;
        }

        public List<Restaurant> SearchRestaurants(
           string keywords = null,
           IList<int> cuisineIds = null,
           IList<int> budgetValues = null, string place = null
           )
        {
            var restaurants = new List<Restaurant>();
            var query = _restaurantRepository.Table;

            //keyword
            if (!String.IsNullOrWhiteSpace(keywords))
            {
                query = from r in query
                        where (r.Name.Contains(keywords)) ||
                        (r.Description.Contains(keywords)) ||
                        ((r.Address.Locality + " " + r.Address.Region + " " + r.Address.Country).Contains(keywords))
                        select r;
            }
            // cuisines

            if (cuisineIds != null && cuisineIds.Count > 0)
            {
                query = from r in query
                        from rc in r.Cuisine.Where(rc => cuisineIds.Contains(rc.CuisineId))
                        select r;
            }

            if (budgetValues != null && budgetValues.Count > 0)
            {
                query = query.Where(r => budgetValues.Contains(r.Budget));
            }
            if (!String.IsNullOrWhiteSpace(place))
            {
                query = query.Where(r => (r.Address.Locality + " " + r.Address.Region + " " + r.Address.Country).Contains(place));
            }

            restaurants = query.ToList();

            return restaurants;
        }



        public List<Restaurant> Search(
           string keywords = null,
           double? Latitude = null,
           double? Longitude = null,
           double? Radius = null,
           IList<int> cuisineIds = null,
           string Sort = null,
           string Order = null
           )
        {
            var restaurants = new List<Restaurant>();
            var query = _restaurantRepository.Table;

            //keyword
            if (!String.IsNullOrWhiteSpace(keywords))
            {
                query = from r in query
                        where (r.Name.Contains(keywords)) ||
                        (r.Description.Contains(keywords)) ||
                        ((r.Address.Locality + " " + r.Address.Region + " " + r.Address.Country).Contains(keywords))
                        select r;
            }
            //radius 


            // cuisines

            if (cuisineIds != null && cuisineIds.Count > 0)
            {
                query = from r in query
                        from rc in r.Cuisine.Where(rc => cuisineIds.Contains(rc.CuisineId))
                        select r;
            }

            //sort

            if (!String.IsNullOrWhiteSpace(Sort))
            {
                if (!string.IsNullOrWhiteSpace(Order) && Order == "ascending")
                {
                    query = from r in query
                            orderby Sort ascending
                            select r;
                }
                else
                {
                    query = from r in query
                            orderby Sort descending
                            select r;
                }
            }

            restaurants = query.ToList();

            return restaurants;
        }


    }
}
