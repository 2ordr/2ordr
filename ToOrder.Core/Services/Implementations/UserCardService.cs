﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Data;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;

namespace ToOrder.Core.Services.Implementations
{
    public class UserCardService : ImplementationBase<IRepository<UserCard>, UserCard>, IUserCardService
    {
         IRepository<UserCard> _userCardRepository;

         public UserCardService(IRepository<UserCard> userCardRepository)
             : base(userCardRepository)
        {
            _userCardRepository = userCardRepository;
        }
    }
}
