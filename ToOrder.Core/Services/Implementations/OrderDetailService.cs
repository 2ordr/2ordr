﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Data;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;

namespace ToOrder.Core.Services.Implementations
{
    public class OrderDetailService : ImplementationBase<IRepository<OrderDetails>, OrderDetails>, IOrderDetailService
    {
        IRepository<OrderDetails> _orderDetailsRepository;

        public OrderDetailService(IRepository<OrderDetails> orderDetailsRepository)
            : base(orderDetailsRepository)
        {
            _orderDetailsRepository = orderDetailsRepository;
        }
    }
}
