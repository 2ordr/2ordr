﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Data;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;

namespace ToOrder.Core.Services.Implementations
{
    public class CuisineService : ImplementationBase<IRepository<Cuisine>, Cuisine>, ICuisineService
    {
        IRepository<Cuisine> _cuisineRepository;

        public CuisineService(IRepository<Cuisine> cuisineRepository)
            : base(cuisineRepository)
        {
            _cuisineRepository = cuisineRepository;
        }
    }
}
