﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Data;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;

namespace ToOrder.Core.Services.Implementations
{
    public class UserFavoriteService : ImplementationBase<IRepository<UserFavorites>, UserFavorites>, IUserFavoriteService
    {
      IRepository<UserFavorites> _userFavoritesRepository;

      public UserFavoriteService(IRepository<UserFavorites> userFavoritesRepository)
          : base(userFavoritesRepository)
        {
            _userFavoritesRepository = userFavoritesRepository;
        }
    }
}
