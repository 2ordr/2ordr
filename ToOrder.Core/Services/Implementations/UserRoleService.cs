﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Data;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;

namespace ToOrder.Core.Services.Implementations
{
  public  class UserRoleService: ImplementationBase<IRepository<UserRole>, UserRole>, IUserRoleService
    {
      IRepository<UserRole> _userRoleRepository;

      public UserRoleService(IRepository<UserRole> userRoleRepository)
          : base(userRoleRepository)
        {
            _userRoleRepository = userRoleRepository;
        }
      public override void AddNew(UserRole entity)
      {
          var count = base.GetAll(u => u.UserId == entity.User.Id && u.RoleId==entity.RoleId).Count();

          if (count == 0)
          {
              base.AddNew(entity);
          }
      }
    }
}
