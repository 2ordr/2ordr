﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Data;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;

namespace ToOrder.Core.Services.Implementations
{
   public  class DinningTableService:ImplementationBase<IRepository<DinningTable>,DinningTable>,IDinningTableService
    {
        IRepository<DinningTable> _dinningTableRepository;

        public DinningTableService(IRepository<DinningTable> dinningTableRepository) : base(dinningTableRepository)
        {
            _dinningTableRepository = dinningTableRepository;
        }
    }
}
