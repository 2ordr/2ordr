﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Data;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;

namespace ToOrder.Core.Services.Implementations
{
    public class ItemService : ImplementationBase<IRepository<Item>, Item>, IItemService
    {
        IRepository<Item> _itemRepository;

        public ItemService(IRepository<Item> itemRepository)
            : base(itemRepository)
        {
            _itemRepository = itemRepository;
        }
    }
}
