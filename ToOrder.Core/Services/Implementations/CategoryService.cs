﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Data;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;

namespace ToOrder.Core.Services.Implementations
{
    public class CategoryService : ImplementationBase<IRepository<Category>, Category>, ICategoryService
    {
        IRepository<Category> _categoryRepository;

        public CategoryService(IRepository<Category> categoryRepository)
            : base(categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

       
    }
}
