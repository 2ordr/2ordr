﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Data;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;

namespace ToOrder.Core.Services.Implementations
{
    public class RoleService : ImplementationBase<IRepository<Role>,Role>, IRoleService
    {
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<UserRole> _userRoleRepository;



        public RoleService(IRepository<Role> roleRepository, 
            IRepository<UserRole> userRoleRepository,
            IRepository<User> userRepository)
            : base(roleRepository)
        {
            _roleRepository = roleRepository;
            _userRoleRepository = userRoleRepository;
            _userRepository = userRepository;
        }




        public IList<Role> GetRolesForUser(string username)
        {
            List<Role> roles = new List<Role>();
            var user = _userRepository.Table.FirstOrDefault(u => u.Email == username);
            if (user == null)
                return roles;

            var query = from m in _userRoleRepository.Table
                        join r in _roleRepository.Table on m.RoleId equals r.Id
                        where m.UserId == user.Id
                        select r;

            return query.ToList();

        }

        public bool IsUserInRole(string username, string role)
        {
            var userRoles = GetRolesForUser(username);
            return userRoles.FirstOrDefault(r => r.Name == role) != null;
        }
    }
}
