﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Data;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;

namespace ToOrder.Core.Services.Implementations
{
    public class MenuItemService : ImplementationBase<IRepository<MenuItem>, MenuItem>, IMenuItemService
    {
        IRepository<MenuItem> _menuItemRepository;

        public MenuItemService(IRepository<MenuItem> menuItemRepository) : base(menuItemRepository)
        {
            _menuItemRepository = menuItemRepository;
        }
    }
}
