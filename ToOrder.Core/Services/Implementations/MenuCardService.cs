﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Data;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;

namespace ToOrder.Core.Services.Implementations
{
    public class MenuCardService : ImplementationBase<IRepository<MenuCard>, MenuCard>, IMenuCardService
    {
        IRepository<MenuCard> _menuCardRepository;

        public MenuCardService(IRepository<MenuCard> menuCardRepository): base(menuCardRepository)
        {
            _menuCardRepository = menuCardRepository;
        }
    }
}
