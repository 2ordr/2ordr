﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Data;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;

namespace ToOrder.Core.Services.Implementations
{
    public class RestaurantCuisineService : ImplementationBase<IRepository<RestaurantCuisine>, RestaurantCuisine>, IRestaurantCuisineService
    {
        IRepository<RestaurantCuisine> _restauranceCuisineRepository;

        public RestaurantCuisineService(IRepository<RestaurantCuisine> restauranceCuisineRepository)
            : base(restauranceCuisineRepository)
        {
            _restauranceCuisineRepository = restauranceCuisineRepository;
        }

    }
}
