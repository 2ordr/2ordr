﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Data;
using ToOrder.Core.Entities;

namespace ToOrder.Core.Services.Implementations
{
    public abstract class ImplementationBase<T,Entity> where T : IRepository<Entity>
        where Entity : BaseEntity
    {
        T _repository;

        public ImplementationBase(T repository)
        {
            _repository = repository; 
        }

        public Entity GetById(int id)
        {
            return _repository.GetById(id);
        }

        public IEnumerable<Entity> GetAll(Func<Entity, bool> predicate)
        {
            return _repository.Table.Where(predicate).AsEnumerable();
        }

        public IEnumerable<Entity> GetAll()  
        {
            return GetAll(x => true);
        }

        public IEnumerable<Entity> GetAll(Func<Entity, bool> predicate,int pageNumber, int pageSize, out int total)
        {
            total = 0;

            var query = _repository.Table.Where(predicate);

            var page = query.OrderBy(entity => entity.Id)
                .Skip((pageNumber -1 ) * pageSize)
                .Take(pageSize)
                .GroupBy(p => new { Total = query.Count() })
                .FirstOrDefault();

            if (page == null)
                return new List<Entity>();

            total = page.Key.Total;
            var entities = page.Select(p => p);

            return entities;
        }

        public IEnumerable<Entity> GetAll(int pageNumber, int pageSize, out int total)
        {
            return this.GetAll(p => true, pageNumber, pageSize, out total);
        }


        public virtual void AddNew(Entity entity)
        {

            try
            {
                _repository.Insert(entity);
                return;

            }
                
            catch (Exception ex)
            {
                if (ex is DbUpdateException)
                    throw new InvalidOperationException("Duplicate data");

                throw;
            }
        }

        public virtual void Update(Entity entity)
        {
            try
            {
                _repository.Update(entity);
            }
            catch (Exception ex)
            {
                if (ex is DbUpdateException)
                    throw new InvalidOperationException("Duplicate data");

                throw;
            }
        }


        public virtual void Update(Entity entity, string[] excludePropertyNames)
        {
            try
            {
                _repository.Update(entity,excludePropertyNames);
            }
            catch (Exception ex)
            {
                if (ex is DbUpdateException)
                    throw new InvalidOperationException("Duplicate data");

                throw;
            }

        }


        public void Delete(Entity entity)
        {
            _repository.Delete(entity);
        }

    }
}
