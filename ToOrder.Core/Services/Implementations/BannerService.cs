﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Data;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;

namespace ToOrder.Core.Services.Implementations
{
    public class BannerService : ImplementationBase<IRepository<Banner>, Banner>, IBannerService
    {
        IRepository<Banner> _bannerRepository;

        public BannerService(IRepository<Banner> bannerRepository)
            : base(bannerRepository)
        {
            _bannerRepository = bannerRepository;
        }

    }
}
