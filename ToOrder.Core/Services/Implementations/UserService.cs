﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Data;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;

namespace ToOrder.Core.Services.Implementations
{
    public class UserService : ImplementationBase<IRepository<User>,User>, IUserService
    {
        IRepository<User> _userRepository;

        public UserService(IRepository<User> userRepository) : base( userRepository)
        {
            _userRepository = userRepository;
        }


        public bool ValidateCredentials(string email, string password)
        {
            if (string.IsNullOrWhiteSpace(email)
                || string.IsNullOrWhiteSpace(password))
                return false;

            return _userRepository.Table.FirstOrDefault(u => u.Email == email && u.Password == password) != null;
        }



        public override void AddNew(User entity)
        {
            var count=base.GetAll(u => u.Email == entity.Email).Count();

            if (count > 0)
                throw new InvalidOperationException("User with this email id already exists"); //todo translate msg

            base.AddNew(entity);
        }
        

        
        

        

       
    }
}
