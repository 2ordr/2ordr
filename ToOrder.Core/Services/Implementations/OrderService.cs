﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Data;
using ToOrder.Core.Entities;
using ToOrder.Core.Services.Interfaces;

namespace ToOrder.Core.Services.Implementations
{
    public class OrderService : ImplementationBase<IRepository<Order>, Order>, IOrderService
    {
        IRepository<Order> _orderRepository;

        public OrderService(IRepository<Order> orderRepository)
            : base(orderRepository)
        {
            _orderRepository = orderRepository;
        }
    }
}
