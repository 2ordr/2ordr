﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrder.Core.Services.Interfaces
{
    public interface IServiceBase<T> 
    {
        T GetById(int id);

        IEnumerable<T> GetAll(Func<T,bool> predicate);
        IEnumerable<T> GetAll();
        IEnumerable<T> GetAll(Func<T,bool> predicate,int pageNumber, int pageSize, out int total);
        IEnumerable<T> GetAll(int pageNumber, int pageSize, out int total);


        void AddNew(T entity);
        
        void Update(T entity);

        void Update(T entity, string[] excludePropertyNames);

        void Delete(T entity);
    }
}
