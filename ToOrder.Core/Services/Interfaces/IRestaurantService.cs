﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Entities;

namespace ToOrder.Core.Services.Interfaces
{
    public interface IRestaurantService : IServiceBase<Restaurant>
    {
        List<Restaurant> SearchRestaurants(
           string keywords = null,
           IList<int> cuisineIds = null,
           IList<int> budgetValues = null,
          string place = null
           );

        List<Restaurant> Search(
            string keywords = null,
            double? Latitude = null,
            double? Longitude = null,
            double? Radius = null,
            IList<int> cuisineIds = null,
            string Sort = null,
            string Order = null
            );
    }
}
