﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Core.Entities;

namespace ToOrder.Core.Services.Interfaces
{
    public interface IRatingService : IServiceBase<Rating>
    {
    }
}
