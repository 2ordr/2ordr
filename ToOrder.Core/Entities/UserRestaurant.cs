﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrder.Core.Entities
{
    public class UserRestaurant : BaseEntity
    {
        [Required]
        public int UserId { get; set; }

        [Required]
        public int RestaurantId { get; set; }

        public virtual User User { get; set; }

        public virtual Restaurant Restaurant { get; set; }

        public string FullName
        {
            get
            {
                return User.FirstName + " " + User.LastName;
            }
        }
    }
}
