﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Localization;

namespace ToOrder.Core.Entities
{
    public class Banner : BaseEntity
    {
        [Display(Name = "FileName", ResourceType = typeof(Resources))]
        public string FileName { get; set; }

        [Display(Name = "Url", ResourceType = typeof(Resources))]
        [DataType(DataType.Url)]
        public string Url { get; set; }

        [Display(Name = "IsActive", ResourceType = typeof(Resources))]
        public bool IsActive { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "FromDate", ResourceType = typeof(Resources))]
        public DateTime? FromDate { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "ToDate", ResourceType = typeof(Resources))]
        public DateTime? ToDate { get; set; }

        public int RestaurantId { get; set; }

        public virtual Restaurant Restaurant { get; set; }
    }
}
