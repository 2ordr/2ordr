﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrder.Core.Entities
{
    public class UserCard : BaseEntity
    {
        [Required]
        [Index(IsUnique = true)]
        [MaxLength(20)]
        public string CardNumber { get; set; }
        [Required]
        public string CardName { get; set; }
        [Required]
        [StringLength(4)]
        public string CardPIN { get; set; }

        [Required]
        public DateTime? CardExpirydate { get; set; }
        public virtual User User { get; set; }

        [Required]
        [ForeignKey("User")]
        public int UserId { get; set; }

    }

}
