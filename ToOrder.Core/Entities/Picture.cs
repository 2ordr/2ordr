﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrder.Core.Entities
{
    public class Picture : BaseEntity
    {
        public string Name { get; set; }
        public string PicturePath { get; set; }
    }
}
