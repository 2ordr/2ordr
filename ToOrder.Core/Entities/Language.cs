﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrder.Core.Entities
{
    public class Language : BaseEntity
    {

        public Language()
        {
            SupportedLanguages = new Dictionary<string, string> 
            {
                { "English","en-us" },
                { "Dansk", "da-dk"}
            };
        }

        public string Culture { get; set; }

        public Dictionary<string,string> SupportedLanguages { get; set; }

    }
}
