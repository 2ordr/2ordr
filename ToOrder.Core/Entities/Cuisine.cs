﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Localization;

namespace ToOrder.Core.Entities
{
    public class Cuisine : BaseEntity
    {
        [Display(Name = "Name", ResourceType = typeof(Resources))]
        [Required(ErrorMessage = "Name cannot be blank"), MaxLength(50)]
        public string Name { get; set; }

        [Display(Name = "CreationDate", ResourceType = typeof(Resources))]
        public DateTime CreationDate { get; set; }
    }
}
