﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrder.Core.Entities
{
    public class OrderDetails : BaseEntity
    {
        public int OrderId { get; set; }

        public int Qty { get; set; }

        public int MenuItemId { get; set; }

        public virtual Order Order { get; set; }

        public virtual MenuItem MenuItem { get; set; }

    }
}
