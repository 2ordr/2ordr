﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Localization;

namespace ToOrder.Core.Entities
{
    public class User : BaseEntity
    {
        public User()
        {
            Address = new Address();

        }

        [Display(Name = "Email", ResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "EmailRequiredError")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }



        [Display(Name = "Password", ResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "PasswordRequiredError")]
        [MinLength(5, ErrorMessage = "Minimum 5 characters")]
        [DataType(DataType.Password)]
        public string Password { get; set; }



        //[Required]
        //[MinLength(5)]
        //[DataType(DataType.Password)]
        //[NotMapped]
        //[Compare("Password", ErrorMessage = "Password and Confirmation Password doesn't match")]
        //public string ConfirmPassword { get; set; }


        [Required]
        [Display(Name = "FirstName", ResourceType = typeof(Resources))]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "LastName", ResourceType = typeof(Resources))]
        public string LastName { get; set; }

        [Display(Name = "Telephone", ResourceType = typeof(Resources))]
        [DataType(DataType.PhoneNumber)]
        public string Telephone { get; set; }

        [Display(Name = "FaxNumber", ResourceType = typeof(Resources))]
        public string FaxNumber { get; set; }

        [Display(Name = "Mobile", ResourceType = typeof(Resources))]
        [DataType(DataType.PhoneNumber)]
        public string Mobile { get; set; }

        [Display(Name = "Website", ResourceType = typeof(Resources))]
        [DataType(DataType.Url)]
        public string Website { get; set; }

        [Display(Name = "Address", ResourceType = typeof(Resources))]
        public Address Address { get; set; }


        [DataType(DataType.Date)]
        [Display(Name = "DOB", ResourceType = typeof(Resources))]
        public DateTime? DOB { get; set; }

        public DateTime? LastUpdated { get; set; }

        public bool IsActivated { get; set; }

        public string ActivationCode { get; set; }

        public string ResetPasswordCode { get; set; }


        public virtual ICollection<UserRole> Roles { get; set; }
    }
}
