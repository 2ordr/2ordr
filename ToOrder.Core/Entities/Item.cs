﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrder.Core.Entities
{
    public class Item : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public Picture Picture { get; set; }

        public int CategoryId { get; set; }

        public decimal Price { get; set; }
    }
}
