﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrder.Core.Entities
{
    public class Payment : BaseEntity
    {
        public int TranId { get; set; }

        public decimal Amount { get; set; }

        public string Details { get; set; }
    }
}
