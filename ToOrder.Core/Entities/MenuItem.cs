﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Localization;

namespace ToOrder.Core.Entities
{
    public class MenuItem : BaseEntity
    {
        [Display(Name = "Name", ResourceType = typeof(Resources))]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Description", ResourceType = typeof(Resources))]
        public string Description { get; set; }

        public int MenuCardId { get; set; }

         [Display(Name = "Price", ResourceType = typeof(Resources))]
        [Required]
        public decimal Price { get; set; }

        public bool IsActive { get; set; }

        [Required]

        [ForeignKey("Category")]
        public int CategoryId { get; set; }   //menuitem will have one and only one category

        public virtual MenuCard MenuCard { get; set; }

        public virtual Category Category { get; set; }

    }
}
