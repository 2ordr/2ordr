﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace ToOrder.Core.Entities
{
    public class RestaurantCuisine : BaseEntity
    {
        [Index("RestaurandAndCuisine",1)]
        public int RestaurantId { get; set; }

        [Index("RestaurandAndCuisine", 2,IsUnique=true)]
        public int CuisineId { get; set; }

        public virtual Cuisine Cuisine { get; set; }
        public virtual Restaurant Restaurant { get; set; }
    }
}
