﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrder.Core.Entities
{
    public class DinningTable : BaseEntity
    {
        [Required]
        public string Name { get; set; }

        public int SeatCount { get; set; }

        
        

        public int MenuCardId { get; set; }

        public virtual MenuCard MenuCard { get; set; }

    }
}
