﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Localization;

namespace ToOrder.Core.Entities
{
    public class Address
    {
        [Display(Name = "Street", ResourceType = typeof(Resources))]
        public string Street { get; set; }
         [Display(Name = "Locality", ResourceType = typeof(Resources))]
        public string Locality { get; set; }
         [Display(Name = "Region", ResourceType = typeof(Resources))]
        public string Region { get; set; }
         [Display(Name = "Country", ResourceType = typeof(Resources))]
        public string Country { get; set; }
    }
}
