﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ToOrder.Localization;

namespace ToOrder.Core.Entities
{
    public class Restaurant : BaseEntity
    {
        [Display(Name = "Name", ResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "NameRequiredError")]
        public string Name { get; set; }

        [Display(Name = "Description", ResourceType = typeof(Resources))]
        public string Description { get; set; }

        [Display(Name = "Address", ResourceType = typeof(Resources))]
        public Address Address { get; set; }

        [Display(Name = "Contact", ResourceType = typeof(Resources))]
        public ContactInfo Contact { get; set; }

        [Range(1, 4)]
        public int Budget { get; set; }

        [Display(Name = "IsSponsored", ResourceType = typeof(Resources))]
        public bool IsSponsored { get; set; }

        public RestaurantStatus Status { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public int Zoom { get; set; }

        [Display(Name = "PictureFileName", ResourceType = typeof(Resources))]
        public string PictureFileName { get; set; }

        public virtual ICollection<RestaurantCuisine> Cuisine { get; set; }

        public virtual ICollection<UserRestaurant> RestaurantUsers { get; set; }

        [Display(Name = "CreationDate", ResourceType = typeof(Resources))]
        public DateTime CreationDate { get; set; }

        public virtual ICollection<Rating> Rating { get; set; }

    }

    public enum RestaurantStatus
    {
        Active,
        Pending,
        Suspended,
    };
}
