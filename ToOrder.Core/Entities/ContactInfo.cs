﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Localization;

namespace ToOrder.Core.Entities
{
    public class ContactInfo
    {
         [Display(Name = "Telephone", ResourceType = typeof(Resources))]
        public string Telephone { get; set; }
         [Display(Name = "FaxNumber", ResourceType = typeof(Resources))]
        public string FaxNumber { get; set; }
         [Display(Name = "Email", ResourceType = typeof(Resources))]
        public string Email { get; set; }
         [Display(Name = "Website", ResourceType = typeof(Resources))]
        public string Website { get; set; }
    }
}
