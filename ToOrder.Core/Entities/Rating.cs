﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Localization;

namespace ToOrder.Core.Entities
{
    public class Rating : BaseEntity
    {
        [Display(Name = "Title", ResourceType = typeof(Resources))]
        public string Title { get; set; }

        [Display(Name = "Description", ResourceType = typeof(Resources))]
        public string Description { get; set; }

        [Display(Name = "Rating", ResourceType = typeof(Resources))]
        [Range(0, 5)]
        public int Ratings { get; set; }

        [Display(Name = "Date", ResourceType = typeof(Resources))]
        public DateTime Date { get; set; }

        public int RestaurantId { get; set; }

        public int UserId { get; set; }

        [Display(Name = "Blocked", ResourceType = typeof(Resources))]
        public bool Blocked { get; set; }

        public virtual Restaurant Restaurant { get; set; }

        public virtual User User { get; set; }
    }
}
