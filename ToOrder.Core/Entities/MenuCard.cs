﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Localization;

namespace ToOrder.Core.Entities
{
    public class MenuCard : BaseEntity
    {
        [Display(Name = "Name", ResourceType = typeof(Resources))]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Description", ResourceType = typeof(Resources))]
        public string Description { get; set; }

        public int RestaurantId { get; set; }

        public virtual Restaurant Restaurant { get; set; }

        public bool IsDefault { get; set; }

    }
}
