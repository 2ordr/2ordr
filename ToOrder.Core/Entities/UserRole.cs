﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToOrder.Core.Entities
{
    public class UserRole : BaseEntity
    {
        [Required]
        public int UserId { get; set; }

        [Required]
        public int RoleId { get; set; }

        public virtual User User {get; set;}

        public virtual Role Role { get; set; }

    
    }
}
