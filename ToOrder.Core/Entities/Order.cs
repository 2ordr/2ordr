﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToOrder.Localization;

namespace ToOrder.Core.Entities
{
    public class Order : BaseEntity
    {
        public int UserId { get; set; }

        public int RestaurantId { get; set; }

        [Display(Name = "Date", ResourceType = typeof(Resources))]
        public DateTime Date { get; set; }

        [Display(Name = "GrossAmount", ResourceType = typeof(Resources))]
        public decimal GrossAmount { get; set; }

        [Display(Name = "Tax", ResourceType = typeof(Resources))]
        public decimal Tax { get; set; }

        [Display(Name = "NetAmount", ResourceType = typeof(Resources))]
        public decimal NetAmount { get; set; }

        public int PaymentId { get; set; }

        public bool IsServed { get; set; }

        public virtual User User { get; set; }

        public virtual Restaurant Restaurant { get; set; }
    }
}
